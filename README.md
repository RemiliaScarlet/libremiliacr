# libremiliacr

libremiliacr is a library that provides utility functions and some extra
batteries to [Crystal](https://crystal-lang.org/).  It is essentially Remilia's
personal standard library, and is similar in nature (and design) to her
[cl-sdm](https://chiselapp.com/user/MistressRemilia/repository/cl-sdm/) library
for Common Lisp.

A list of releases and release notes can be found
[here](https://chiselapp.com/user/MistressRemilia/repository/libremiliacr/wiki?name=releases).

Wanna support Remilia? [Buy me a coffee on Ko-Fi](https://ko-fi.com/L4L614QNC),
or support me through Liberapay.

<a href='https://ko-fi.com/L4L614QNC' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>
<a href="https://liberapay.com/RemiliaScarlet/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

## Documentation

Documentation can be found [here](https://remilia.sdf.org/programming/docs/libremiliacr).

## Version Control

libremiliacr sources are managed using [Fossil](https://www.fossil-scm.org/), a
distributed version control system. The [Fossil
repository](https://chiselapp.com/user/MistressRemilia/repository/libremiliacr/)
contains the urtext, tickets, and wiki.

If you are reading this on GitLab or some other Git repository or service, then
you are looking at a mirror. The names of check-ins and other artifacts in a Git
mirror are different from the official names for those objects. The official
names for check-ins are found in a footer on the check-in comment for authorized
mirrors. The official check-in name can also be seen in the manifest.uuid file
in the root of the tree. Always use the official name, not the Git-name, when
communicating about a libremiliacr check-in.

## Installation

Add this to your application's shard.yml:

```yaml
dependencies:
  libremiliacr:
    fossil: https://chiselapp.com/user/MistressRemilia/repository/libremiliacr
```

## Stuff Included

Most of this stuff has been added out of need, or occasionally just to have some
syntactic sugar.

* Command line parser.
* Bit reader (read bits at a time instead of bytes).
* Array Pool (reuse arrays to keep memory allocations down).
* `assert` macro that can have its code inside disabled at compile time
  (`-Dremilib_no_assertions`).
* BZip2 compression and decompression.
* String wrapping.
* An alternative/very simple logging framework, including concurrent logging.
* Extra ANSI terminal handling.
* Detection of the
  [Terminology](https://www.enlightenment.org/about-terminology) terminal
  emulator.
* Console-based progress bar.
* Dependency Graph.
* Common Lisp-style string
  [formatting](https://gigamonkeys.com/book/a-few-format-recipes.html).
* Job pool (a way to spread jobs out over a limited number of Fibers).
* Multiply-Add functions and fast math approximation functions.
* Conversion to and from Common Lisp's ["universal
  time"](http://www.lispworks.com/documentation/HyperSpec/Body/25_adb.htm).
* A bunch of monkey patched extensions to the standard library that I've found I
  needed or were useful over time (some of these will be moved into the
  `RemiLib` module eventually).
* Native CRC64 implementation.
* Native xxHash3, xxHash128, xxHash64, and xxHash32 implementations.
* Native SpookyHash implementation.
* `MmappedFile` class, which allows you to do high-speed read/write operations
  on `mmap()`ed files in a stream-like way..
* Configuration and Data File management.

## How do I contribute?

1. Go to [https://chiselapp.com/user/MistressRemilia/repository/libremiliacr/](https://chiselapp.com/user/MistressRemilia/repository/libremiliacr/)
   and clone the Fossil repository.
2. Create a new branch for your feature.
3. Push locally to the new branch.
4. Create a [bundle](https://fossil-scm.org/home/help?cmd=bundle) with Fossil
   that contains your changes.
5. Get in contact with me.
4. Contact me to set up a merge.

* Remilia Scarlet - creator and maintainer
  * Homepage: [https://remilia.sdf.org/](https://remilia.sdf.org/)
  * Mastodon: [@MistressRemilia@social.sdf.org](https://social.sdf.org/@MistressRemilia)
  * Email: zremiliaz@postzeoz.jpz  My real address does not contain Z's

## Links and Licenses

libremiliacr is under the
[GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.html),
except for the BZip2 code and xxHash code which is under the MIT License, and the
SpookyHash code which is under a BSD 3-Clause License.
