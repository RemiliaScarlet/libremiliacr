require "rake/clean"

LIBRARY_SOURCES = Rake::FileList["src/**/*.cr"]
SPEC_SOURCES = Rake::FileList["spec/**/*.cr"]
BZIP_DEC_EXAMPLE = Rake::FileList["examples/bzip2Dec.cr"]
BZIP_ENC_EXAMPLE = Rake::FileList["examples/bzip2Enc.cr"]
XXHASH_EXAMPLE = Rake::FileList["examples/xxhash.cr"]
XXHASH_STREAM_EXAMPLE = Rake::FileList["examples/xxhashstream.cr"]
SPOOKY_EXAMPLE = Rake::FileList["examples/spookyhash.cr"]
DOCS_DIR = "docs"

EXAMPLES = ["bzip2Dec", "bzip2Enc", "xxhash", "xxhashstream", "spookyhash"]

file "bzip2Dec" => LIBRARY_SOURCES + BZIP_DEC_EXAMPLE do
  sh "crystal build -p #{BZIP_DEC_EXAMPLE}"
end
CLOBBER << "bzip2Dec"

file "bzip2Enc" => LIBRARY_SOURCES + BZIP_ENC_EXAMPLE do
  sh "crystal build -p #{BZIP_ENC_EXAMPLE}"
end
CLOBBER << "bzip2Enc"

file "xxhash" => LIBRARY_SOURCES + XXHASH_EXAMPLE do
  sh "crystal build -p #{XXHASH_EXAMPLE}"
end
CLOBBER << "xxhash"

file "xxhashstream" => LIBRARY_SOURCES + XXHASH_STREAM_EXAMPLE do
  sh "crystal build -p #{XXHASH_STREAM_EXAMPLE}"
end
CLOBBER << "xxhashstream"

file "spookyhash" => LIBRARY_SOURCES + SPOOKY_EXAMPLE do
  sh "crystal build -p #{SPOOKY_EXAMPLE}"
end
CLOBBER << "spookyhash"

desc "Build documentation"
directory DOCS_DIR => LIBRARY_SOURCES do
  sh "crystal docs --project-name=libremiliacr --source-url-pattern='https://chiselapp.com/user/MistressRemilia/repository/libremiliacr/file?name=%{path}&ci=%{refname}&ln=%{line}' --source-refname=tip"
end
CLOBBER << DOCS_DIR

task default: [:examples, DOCS_DIR]

desc "Build example programs"
task examples: EXAMPLES

desc "Run unit tests"
task :test => LIBRARY_SOURCES + SPEC_SOURCES do
  sh "crystal spec -p"
end

desc "Run Ameba linter"
task :lint do
  sh "ameba"
end
