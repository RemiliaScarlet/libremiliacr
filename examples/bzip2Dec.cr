####
#### Example BZip2 file decompressor.
####
require "../src/libremiliacr"

if ARGV.size != 2
  puts "Usage: #{Path[PROGRAM_NAME].basename} <bzip2 file> <outfile>"
  puts "This will NOT clobber existing files."
  exit 0
end

inFile = ARGV[0]
outFile = ARGV[1]

abort "Input file does not exist" unless File.exists?(inFile)
abort "Output file already exists, will not clobber" if File.exists?(outFile)

File.open(inFile, "rb") do |file|
  File.open(outFile, "wb") do |outStrm|
    RemiLib::Compression::BZip2::Reader.open(file) do |bzio|
      IO.copy(bzio, outStrm)
    end
  end
end
