####
#### Example BZip2 file compressor..
####
require "../src/libremiliacr"

if ARGV.size < 2 || ARGV.size > 3
  puts "Usage: #{Path[PROGRAM_NAME].basename} <infile> <bzip2 file> [clobber]"
  puts "This will NOT clobber existing files unless [clobber] is the string 'yes'"
  exit 0
end

inFile = ARGV[0]
outFile = ARGV[1]
overwrite : Bool = (ARGV[2]?.try &.==("yes")) || false

abort "Input file does not exist" unless File.exists?(inFile)
abort "Output file already exists, will not clobber" if File.exists?(outFile) && !overwrite

File.open(inFile, "rb") do |file|
  File.open(outFile, "wb") do |outStrm|
    RemiLib::Compression::BZip2::Writer.open(outStrm) do |bzio|
      IO.copy(file, bzio)
    end
  end
end
