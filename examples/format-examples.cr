require "../src/remilib/format"

################################################################################
###
### Basic Formatting
###

# Should print:
#  boobs
puts RemiLib::Format::Formatter.new("~a").format(:boobs)

# Should print:
#  boobs
puts RemiLib::Format::Formatter.new("~a").format("boobs")

# Should print:
#  "boobs"
puts RemiLib::Format::Formatter.new("~s").format("boobs")

################################################################################
###
### Integer Formatting
###

# Should print:
#  6969 = 6969 decimal
puts RemiLib::Format::Formatter.new("6969 = ~d decimal").format(6969)

# Should print:
#  6969 = 6,969 decimal
puts RemiLib::Format::Formatter.new("6969 = ~:d decimal").format(6969)

# Should print:
#  6969 = +6969 decimal
puts RemiLib::Format::Formatter.new("6969 = ~@d decimal").format(6969)

# Should print:
#  6969 = +6,969 decimal
puts RemiLib::Format::Formatter.new("6969 = ~@:d decimal").format(6969)

# Should print:
#   6969 = 1b39 hex
puts RemiLib::Format::Formatter.new("6969 = ~x hex").format(6969)

# Should print:
#   6969 = 1b39 hex
puts RemiLib::Format::Formatter.new("6969 = ~:x hex").format(6969)

# Should print:
#   6969 = +1b39 hex
puts RemiLib::Format::Formatter.new("6969 = ~@x hex").format(6969)

# Should print:
#   6969 = +1,b39 hex
puts RemiLib::Format::Formatter.new("6969 = ~@:x hex").format(6969)

# Should print:
#   6969 = 15471 octal
puts RemiLib::Format::Formatter.new("6969 = ~o octal").format(6969)

# Should print:
#   6969 = 15,471 octal
puts RemiLib::Format::Formatter.new("6969 = ~:o octal").format(6969)

# Should print:
#   6969 = +15471 octal
puts RemiLib::Format::Formatter.new("6969 = ~@o octal").format(6969)

# Should print:
#   6969 = +15,471 octal
puts RemiLib::Format::Formatter.new("6969 = ~@:o octal").format(6969)

# Should print:
#   6969 = 1101100111001 binary
puts RemiLib::Format::Formatter.new("6969 = ~b binary").format(6969)

# Should print:
#   6969 = 1,101,100,111,001 binary
puts RemiLib::Format::Formatter.new("6969 = ~:b binary").format(6969)

# Should print:
#   6969 = +1101100111001 binary
puts RemiLib::Format::Formatter.new("6969 = ~@b binary").format(6969)

# Should print:
#   6969 = +1,101,100,111,001 binary
puts RemiLib::Format::Formatter.new("6969 = ~@:b binary").format(6969)

################################################################################
###
### English/Roman Formatting
###

# Should print
#   6969 = six thousand, nine hundred sixty-nine
puts RemiLib::Format::Formatter.new("6969 = ~r").format(6969)

# Should print
#   sixty-ninth
#puts RemiLib::Format::Formatter.new("69 = ~:r").format(69) # Not yet supported

# Should print
#   69 = LXIX
puts RemiLib::Format::Formatter.new("69 = ~@r").format(69)

################################################################################
###
### Iteration
###

# Should print
#   1-2-3-4-5-69
#puts RemiLib::Format::Formatter.new("~{~a~^-~}").format([1, 2, 3. 4, 5, 69], :a, :b) # Iteration yet working

################################################################################
###
### Jumping ARound
###

# Should print:
#   69 pirates
puts RemiLib::Format::Formatter.new("~a pirate~:p").format(69)

# Should print:
#   1 ninja
puts RemiLib::Format::Formatter.new("~a ninja~:p").format(1)

# Should print:
#   69 pinkies
puts RemiLib::Format::Formatter.new("~2*~a pink~@:p").format(42, 36, 69)

# Should print:
#   1 pinky
#     yep
#
#
#     indeed, three newlines before this, and the first format number was 42
puts RemiLib::Format::Formatter.new("~*~a pink~@:p~%  yep~3%  indeed, three newlines before this, "\
                                    "and the first format number was ~2:*~a").format(42, 1)

################################################################################
###
### Conditionals
###

# Should print
#   100 is: mucho
format("0 is: ~[cero~;uno~;dos~:;mucho~]~%", 0)

# Should print
#   100 is: mucho
format("1 is: ~[cero~;uno~;dos~:;mucho~]~%", 1)

# Should print
#   100 is: mucho
format("2 is: ~[cero~;uno~;dos~:;mucho~]~%", 2)

# Should print
#   100 is: mucho
format("100 is: ~[cero~;uno~;dos~:;mucho~]~%", 100)

# Should print:
#   x = 10
format("~@[x = ~a~]~@[y = ~a~]~%", 10, nil)

# Should print:
#   y = 2-
format("~@[x = ~a~]~@[y = ~a~]~%", nil, 20)

# Should print:
#   x = 10, y = 20
format("~@[x = ~a~]~@[, y = ~a~]", 10, 20)

# Should print:
#   I saw zero elves.
format("I saw ~r el~:*~[ves~;f~:;ves~].~%", 0)

# Should print:
#   I saw one elf.
format("I saw ~r el~:*~[ves~;f~:;ves~].~%", 1)

# Should print:
#   I saw two elves.
format("I saw ~r el~:*~[ves~;f~:;ves~].~%", 2)

# Should print:
#   1 == 2 ? No!
puts RemiLib::Format::Formatter.new("1 == 2 ? ~:[No!~;Yes?~]").format(1 == 2)

# Should print:
#   1 == 1 ? Yes!
puts RemiLib::Format::Formatter.new("1 == 1 ? ~:[No?~;Yes!~]").format(1 == 1)

################################################################################
###
### A few other tests
###

# Should print:
#   This is from the module to STDERR, and 'true' should be true
RemiLib::Format.format(STDERR, "This is from the module to STDERR, and '~s' should be true~%", true)

# Should print:
#   And this one is from the toplevel.  69 should be 69
#   And this should be on a new line
format("And this one is from the toplevel.  ~a should be 69~%", 69) << "And this should be on a new line\n"

# Should print:
#   9 tildes: ~~~~~~~~~
puts RemiLib::Format::Formatter.new("9 tildes: ~9~").format()
