####
#### A very simple hashing program that uses SpookyHash.
####
require "../src/libremiliacr"

# Check command line
if ARGV.empty? || ARGV.includes?("--help") || ARGV.includes?("-h")
  puts %|Usage: spookyhash <file> [size] [stream?]

[size] is optional and defaults to 128.  It can be one of: 128, 64, 32.

If [stream?] is equal to 'yes' (minus quotes), then the file is streamed and the
hash is computed incrementally.  Otherwise, the entire file is read into RAM
before hashing.|
  exit 0
end

stream = ARGV.size >= 3 && ARGV[2] == "yes"
size = (ARGV[1]?.try &.to_i32) || 128

if stream
  buf = Slice(UInt8).new(4096, 0u8)
  hasher = RemiLib::Digest::SpookyHash.new

  File.open(ARGV[0], "rb") do |file|
    while (numRead = file.read(buf)) > 0
      hasher.update(buf[...numRead])
    end

    case size
    when 128 then printf("%032x\n", hasher.hash)
    when 64 then printf("%016x\n", hasher.hash64)
    when 32 then printf("%08x\n", hasher.hash32)
    else abort "Bad hash size"
    end
  end
else
  data = File.read(ARGV[0]).to_slice # This is faster than getb_to_end
  case size
  when 128 then printf("%032x\n", RemiLib::Digest::SpookyHash.calculate128(data))
  when 64 then printf("%016x\n", RemiLib::Digest::SpookyHash.calculate64(data))
  when 32 then printf("%08x\n", RemiLib::Digest::SpookyHash.calculate32(data))
  else abort "Bad hash size"
  end
end
