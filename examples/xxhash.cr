####
#### A very simple hashing program that uses xxHash.  NOTE: This reads the
#### entire file into RAM!
####
require "../src/libremiliacr"

# Check command line
if ARGV.empty? || ARGV.includes?("--help") || ARGV.includes?("-h")
  puts %|Usage: xxhash <file> [algorithm]

[algorithm] is optional and defaults to 3.  It can be one of:
   3 : XXH3 64-bit
 128 : XXH3 128-bit
  64 : XXH64
  32 : XXH32

Note that <file> will be read into RAM in its entirety.|
  exit 0
end

# Read the file into RAM
data = File.read(ARGV[0]).to_slice # This is faster than getb_to_end

# Figure out which algorithm we want.
alg = case ARGV[1]?
      when nil, "3" then :xxh3
      when "128" then :xxh128
      when "64" then :xxh64
      when "32" then :xxh32
      else abort "Bad algorithm selection"
      end

# Hash and print.
case alg
when :xxh3 then printf("%016x\n", RemiLib::Digest::XXHash3.calculate(data))
when :xxh128 then printf("%032x\n", RemiLib::Digest::XXHash128.calculate(data))
when :xxh64 then printf("%016x\n", RemiLib::Digest::XXHash64.calculate(data))
when :xxh32 then printf("%08x\n", RemiLib::Digest::XXHash32.calculate(data))
else abort "Algorithm not supported"
end
