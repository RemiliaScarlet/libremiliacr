####
#### A very simple hashing program that uses xxHash.  This uses the streaming
#### API, which is significantly slower.
####
require "../src/libremiliacr"

# Check command line
if ARGV.empty? || ARGV.includes?("--help") || ARGV.includes?("-h")
  puts %|Usage: xxhash <file> [algorithm]

[algorithm] is optional and defaults to 64.  It can be one of:
   3 : XXH3 64-bit
 128 : XXH3 128-bit
  64 : XXH64
  32 : XXH32

Note that this example uses the streaming API, which is significantly slower.|
  exit 0
end

# Figure out which algorithm we want.
alg = case ARGV[1]?
      when nil, "64" then :xxh64
      when "128" then :xxh128
      when "3" then :xxh3
      when "32" then :xxh32
      else abort "Bad algorithm selection"
      end

File.open(ARGV[0], "rb") do |file|
  case alg
  when :xxh32 then printf("%08x\n", RemiLib::Digest::XXHash32.calculate(file))
  when :xxh64 then printf("%016x\n", RemiLib::Digest::XXHash64.calculate(file))
  else abort "Selected algorithm does not yet support streaming"
  end
end
