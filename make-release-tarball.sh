#!/bin/bash

set -e

if [[ -z "$1" ]]; then
    echo "Usage: `basename "$0"` <version without v>"
    exit 1
fi

fossil tarball v$1 libremiliacr-$1.tar.gz --name libremiliacr-$1
gunzip libremiliacr-$1.tar.gz
bzip2 -9 libremiliacr-$1.tar
tar tvjf libremiliacr-$1.tar.bz2

echo "SHA256: $(sha256sum libremiliacr-$1.tar.bz2 | cut -d ' ' -f 1)"
echo "MD5: $(md5sum libremiliacr-$1.tar.bz2 | cut -d ' ' -f 1)"
echo "Size: $(stat -c '%s' libremiliacr-$1.tar.bz2) bytes"
echo "Created libremiliacr-$1.tar.bz2"
