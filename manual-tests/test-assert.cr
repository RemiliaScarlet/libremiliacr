require "../src/libremiliacr"

RemiLib.assert(5 == 5)

begin
  RemiLib.assert(5 == 4)
rescue err : RemiLib::AssertionError
  puts "Assertion succeeded, error message: #{err}"
end

RemiLib.assert("This should have succeeded", 5 == 5)
RemiLib.assert("This failed successfully", 5 == 4)

puts "hi"
