require "../src/libremiliacr"

SPEED = (ARGV[0]?.try &.to_i?.try &.milliseconds) || 42.milliseconds

puts "== Normal Test =="
RemiLib::Console.withProgress("Test", 100) do |bar|
  100.times do |_|
    sleep SPEED
    bar.pump
  end
end

puts "== Normal Test (without withProgress) =="
pbar = RemiLib::Console::ProgressBar.new("Test", 100)
100.times do |_|
  sleep SPEED
  pbar.pump
end

puts "\n== Normal Test with Post-bar Label =="
RemiLib::Console.withProgress("Test", 100) do |bar|
  bar.postLabel = "Post Label"
  100.times do |_|
    sleep SPEED
    bar.pump
  end
end

puts "\n== Clip Long Label =="
RemiLib::Console.withProgress("Test with a really large label", 100) do |bar|
  bar.labelWidth = 8
  100.times do |_|
    sleep SPEED
    bar.pump
  end
end

puts "\n== Clip Long Label + Post-bar Label =="
RemiLib::Console.withProgress("Test with a really large label", 100) do |bar|
  bar.labelWidth = 8
  bar.postLabel = "Post Label"
  100.times do |_|
    sleep SPEED
    bar.pump
  end
end

puts "\n== Clip Long Label + Clipped Long Post-bar Label =="
RemiLib::Console.withProgress("Test with a really large label", 100) do |bar|
  bar.labelWidth = 8
  bar.postLabelWidth = 8
  bar.postLabel = "Post Label that is really long"
  100.times do |_|
    sleep SPEED
    bar.pump
  end
end

puts "\n== Normal Label + Clipped Long Post-bar Label =="
RemiLib::Console.withProgress("Test", 100) do |bar|
  bar.postLabelWidth = 8
  bar.postLabel = "Post Label that is really long"
  100.times do |_|
    sleep SPEED
    bar.pump
  end
end

puts "\n== Test With #noAutoRefresh =="
RemiLib::Console.withProgress("Test", 100) do |bar|
  bar.noAutoRefresh = true
  100.times do |i|
    sleep SPEED
    bar.pump

    bar.refresh if i % 5 == 0
  end
end

puts "\n== Test #done =="
RemiLib::Console.withProgress("Test", 100) do |bar|
  100.times do |_|
    sleep SPEED
    bar.pump
  end

  bar.done

  begin
    bar.pump
    abort "FAILED, did not raise the proper exception"
  rescue err : RemiLib::Console::ProgressBar::ProgressBarDoneError
    puts "Error successfully received: #{err}"
  end
end

puts "\n== Test #done Called Multiple Times =="
RemiLib::Console.withProgress("Test", 100) do |bar|
  100.times do |_|
    sleep SPEED
    bar.pump
  end

  bar.done
  bar.done
  bar.done
end

puts "\n== Clip Long Label (no ellipses) =="
RemiLib::Console.withProgress("Test with a really large label", 100) do |bar|
  bar.labelWidth = 8
  bar.addEllipses = false
  100.times do |_|
    sleep SPEED
    bar.pump
  end
end

puts "\n== Clip Long Label + Post-bar Label (no ellipses) =="
RemiLib::Console.withProgress("Test with a really large label", 100) do |bar|
  bar.labelWidth = 8
  bar.postLabel = "Post Label"
  bar.addEllipses = false
  100.times do |_|
    sleep SPEED
    bar.pump
  end
end

puts "\n== Clip Long Label + Clipped Long Post-bar Label (no ellipses) =="
RemiLib::Console.withProgress("Test with a really large label", 100) do |bar|
  bar.labelWidth = 8
  bar.postLabelWidth = 8
  bar.postLabel = "Post Label that is really long"
  bar.addEllipses = false
  100.times do |_|
    sleep SPEED
    bar.pump
  end
end

puts "\n== Normal Test (Float32) =="
RemiLib::Console.withProgress("Test", 100.0_f32) do |bar|
  200.times do |i|
    sleep SPEED
    bar.step = i / 2_f32 + 0.5
  end
end

puts "\n== Normal Test (Float64) =="
RemiLib::Console.withProgress("Test", 100.0_f32) do |bar|
  200.times do |i|
    sleep SPEED
    bar.step = i / 2_f32 + 0.5
  end
end

puts "\n== Normal Test (BigNum) =="
RemiLib::Console.withProgress("Test", 100.to_big_i) do |bar|
  200.to_big_i.times do |i|
    sleep SPEED
    bar.step = i
  end
end

puts "\n== Test Over 100% =="
RemiLib::Console.withProgress("Test", 100) do |bar|
  bar.allowOver100 = true

  100.times do |_|
    sleep SPEED
    bar.pump
  end

  12.times do |_|
    sleep SPEED
    bar.pump
  end
end

puts "\n== Test Over 100% With Percent Cap =="
RemiLib::Console.withProgress("Test", 100) do |bar|
  bar.allowOver100 = false

  100.times do |_|
    sleep SPEED
    bar.pump
  end

  12.times do |_|
    sleep SPEED
    bar.pump
  end
end
