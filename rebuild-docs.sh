#!/bin/bash

DOCBASEDIR="$(dirname "$0")/doc"
BASEURL="https://nanako.mooo.com/libremiliacr/doc/ckout/doc/api/"
URLPATTERN="https://nanako.mooo.com/libremiliacr/file?name=%{path}&ci=%{refname}&ln=%{line}"

echo "*** Removing old documentation ***"
rm -rfv "$DOCBASEDIR"

echo "*** Old documentation removed, generating new ***"
crystal docs \
        -o "$DOCBASEDIR" \
        --source-refname=tip \
        --sitemap-base-url="$BASEURL" \
        --source-url-pattern="$URLPATTERN"

echo "*** Documentation built ***"
