require "./spec_helper"

def makeParser : RemiLib::Args::ArgParser
  RemiLib::Args::ArgParser.new("test", "0.1.0")
end

describe "Argument Types" do
  it "Creates Arguments" do
    flag = RemiLib::Args::FlagArgument.new("flag")
    flag.longName.should eq "--flag"
    flag.shortName.should be_nil
    flag.group.should be_empty
    flag.help.should be_empty
    flag.called?.should be_false
    flag.str.should eq false.to_s

    flag = RemiLib::Args::FlagArgument.new("flag2", 'f')
    flag.longName.should eq "--flag2"
    flag.shortName.should eq 'f'
    flag.group.should be_empty
    flag.help.should be_empty
    flag.called?.should be_false
    flag.str.should eq false.to_s

    flag = RemiLib::Args::FlagArgument.new("flag2", 'f', group: "Group")
    flag.longName.should eq "--flag2"
    flag.shortName.should eq 'f'
    flag.group.should eq "Group"
    flag.help.should be_empty
    flag.called?.should be_false
    flag.str.should eq false.to_s

    flag = RemiLib::Args::FlagArgument.new("flag2", 'f', group: "Group", help: "Help")
    flag.longName.should eq "--flag2"
    flag.shortName.should eq 'f'
    flag.group.should eq "Group"
    flag.help.should eq "Help"
    flag.called?.should be_false
    flag.str.should eq false.to_s
  end

  it "Creates MultiFlag arguments" do
    flag = RemiLib::Args::MultiFlagArgument.new("flag", 'f', group: "Group", help: "Help")
    flag.longName.should eq "--flag"
    flag.shortName.should eq 'f'
    flag.group.should eq "Group"
    flag.help.should eq "Help"
    flag.called?.should be_false
    flag.str.should eq "0"
    flag.times.should eq 0
  end

  it "Creates String arguments" do
    str = RemiLib::Args::StringArgument.new("str", 'f', group: "Group", help: "Help")
    str.longName.should eq "--str"
    str.shortName.should eq 'f'
    str.group.should eq "Group"
    str.help.should eq "Help"
    str.called?.should be_false
    str.str.should be_empty
    str.value.should be_empty
  end

  it "Creates MultiString arguments" do
    str = RemiLib::Args::MultiStringArgument.new("str", 'f', group: "Group", help: "Help")
    str.longName.should eq "--str"
    str.shortName.should eq 'f'
    str.group.should eq "Group"
    str.help.should eq "Help"
    str.called?.should be_false
    str.str.should eq "[]"
    str.values.should eq [] of Array(String)
    str.times.should eq 0
  end

  it "Creates Int arguments" do
    arg = RemiLib::Args::IntArgument.new("int", 'i', group: "Group", help: "Help")
    arg.longName.should eq "--int"
    arg.shortName.should eq 'i'
    arg.group.should eq "Group"
    arg.help.should eq "Help"
    arg.called?.should be_false
    arg.str.should eq "0"
    arg.value.should eq 0
    arg.minimum.should eq Int64::MIN
    arg.maximum.should eq Int64::MAX
  end

  it "Creates MultiInt arguments" do
    arg = RemiLib::Args::MultiIntArgument.new("int", 'i', group: "Group", help: "Help")
    arg.longName.should eq "--int"
    arg.shortName.should eq 'i'
    arg.group.should eq "Group"
    arg.help.should eq "Help"
    arg.called?.should be_false
    arg.str.should eq "[]"
    arg.values.should eq [] of Array(Int64)
    arg.times.should eq 0
  end

  it "Creates Float arguments" do
    arg = RemiLib::Args::FloatArgument.new("float", 'i', group: "Group", help: "Help")
    arg.longName.should eq "--float"
    arg.shortName.should eq 'i'
    arg.group.should eq "Group"
    arg.help.should eq "Help"
    arg.called?.should be_false
    arg.str.should eq 0.0f64.to_s
    arg.value.should eq 0
    arg.minimum.should eq Float64::MIN
    arg.maximum.should eq Float64::MAX
  end

  it "Creates MultiFloat arguments" do
    arg = RemiLib::Args::MultiFloatArgument.new("float", 'i', group: "Group", help: "Help")
    arg.longName.should eq "--float"
    arg.shortName.should eq 'i'
    arg.group.should eq "Group"
    arg.help.should eq "Help"
    arg.called?.should be_false
    arg.str.should eq "[]"
    arg.values.should eq [] of Array(Float64)
    arg.times.should eq 0
  end

  it "StringArgument sets values" do
    arg = RemiLib::Args::StringArgument.new("str", 'i', group: "Group", help: "Help")
    arg.value = "foo"
    arg.value.should eq "foo"
    arg.called?.should be_true

    arg = RemiLib::Args::StringArgument.new("str", 'i', group: "Group", help: "Help")
    arg.value = 1
    arg.value.should eq "1"
    arg.called?.should be_true

    arg = RemiLib::Args::StringArgument.new("str", 'i', group: "Group", help: "Help")
    arg.setValue!("foo")
    arg.value.should eq "foo"
    arg.called?.should be_false

    arg = RemiLib::Args::StringArgument.new("str", 'i', group: "Group", help: "Help")
    arg.setValue!(1)
    arg.value.should eq "1"
    arg.called?.should be_false
  end

  it "IntArgument sets values" do
    arg = RemiLib::Args::IntArgument.new("int", 'i', group: "Group", help: "Help")
    arg.value = 69
    arg.value.should eq 69
    arg.called?.should be_true

    arg = RemiLib::Args::IntArgument.new("int", 'i', group: "Group", help: "Help")
    arg.value = "36"
    arg.value.should eq 36
    arg.called?.should be_true

    arg = RemiLib::Args::IntArgument.new("int", 'i', group: "Group", help: "Help")
    arg.setValue!(42)
    arg.value.should eq 42
    arg.called?.should be_false

    arg = RemiLib::Args::IntArgument.new("int", 'i', group: "Group", help: "Help")
    arg.setValue!("42")
    arg.value.should eq 42
    arg.called?.should be_false
  end

  it "FloatArgument sets values" do
    arg = RemiLib::Args::FloatArgument.new("float", 'i', group: "Group", help: "Help")
    arg.value = 69.0
    arg.value.should eq 69.0
    arg.called?.should be_true

    arg = RemiLib::Args::FloatArgument.new("float", 'i', group: "Group", help: "Help")
    arg.value = "36.0"
    arg.value.should eq 36.0
    arg.called?.should be_true

    arg = RemiLib::Args::FloatArgument.new("float", 'i', group: "Group", help: "Help")
    arg.setValue!(42.0)
    arg.value.should eq 42.0
    arg.called?.should be_false

    arg = RemiLib::Args::FloatArgument.new("float", 'i', group: "Group", help: "Help")
    arg.setValue!("42.0")
    arg.value.should eq 42.0
    arg.called?.should be_false
  end

  it "MultiStringArgument sets values" do
    arg = RemiLib::Args::MultiStringArgument.new("str", 'i', group: "Group", help: "Help")
    arg << "foo"
    arg.values.size.should eq 1
    arg.values[0].should eq "foo"
    arg.called?.should be_true
    arg.times.should eq 1

    arg << "lol"
    arg.values.size.should eq 2
    arg.values[1].should eq "lol"
    arg.called?.should be_true
    arg.times.should eq 2

    arg.values = ["I", "love", "Crystal"]
    arg.values.size.should eq 3
    arg.values.should eq ["I", "love", "Crystal"]
    arg.called?.should be_true
    arg.times.should eq 3

    arg = RemiLib::Args::MultiStringArgument.new("str", 'i', group: "Group", help: "Help")
    arg.setValues!(["I", "love", "Crystal"])
    arg.values.size.should eq 3
    arg.values.should eq ["I", "love", "Crystal"]
    arg.called?.should be_false
    arg.times.should eq 0
  end

  it "MultiIntArgument sets values" do
    arg = RemiLib::Args::MultiIntArgument.new("int", 'i', group: "Group", help: "Help")
    arg << 1
    arg.values.size.should eq 1
    arg.values[0].should eq 1
    arg.called?.should be_true
    arg.times.should eq 1

    arg << "2"
    arg.values.size.should eq 2
    arg.values[1].should eq 2
    arg.called?.should be_true
    arg.times.should eq 2

    arg.values = [69i64, 42i64, 36i64]
    arg.values.size.should eq 3
    arg.values.should eq [69i64, 42i64, 36i64]
    arg.called?.should be_true
    arg.times.should eq 3

    arg = RemiLib::Args::MultiIntArgument.new("str", 'i', group: "Group", help: "Help")
    arg.setValues!([69i64, 42i64, 36i64])
    arg.values.size.should eq 3
    arg.values.should eq [69i64, 42i64, 36i64]
    arg.called?.should be_false
    arg.times.should eq 0
  end

  it "MultiFloatArgument sets values" do
    arg = RemiLib::Args::MultiFloatArgument.new("float", 'i', group: "Group", help: "Help")
    arg << 1.0
    arg.values.size.should eq 1
    arg.values[0].should eq 1.0
    arg.called?.should be_true
    arg.times.should eq 1

    arg << "2.0"
    arg.values.size.should eq 2
    arg.values[1].should eq 2.0
    arg.called?.should be_true
    arg.times.should eq 2

    arg.values = [4.0, 5.0, 69.42]
    arg.values.size.should eq 3
    arg.values.should eq [4.0, 5.0, 69.42]
    arg.called?.should be_true
    arg.times.should eq 3

    arg = RemiLib::Args::MultiFloatArgument.new("str", 'i', group: "Group", help: "Help")
    arg.setValues!([4.0, 5.0, 69.42])
    arg.values.size.should eq 3
    arg.values.should eq [4.0, 5.0, 69.42]
    arg.called?.should be_false
    arg.times.should eq 0
  end

  it "StringArgument sets constraints" do
    arg = RemiLib::Args::StringArgument.new("str", 'i', group: "Group", help: "Help")
    arg.oneOf = ["foo", "bar"]
    arg.value = "foo"
    arg.value.should eq "foo"
    arg.called?.should be_true

    arg.value = "bar"
    arg.value.should eq "bar"
    arg.called?.should be_true

    expect_raises(RemiLib::Args::ArgumentError) do
      arg.value = "baz"
    end
    arg.value.should eq "bar"
    arg.called?.should be_true

    arg.setValue!("baz")
    arg.value.should eq "baz"
  end

  it "MultiStringArgument sets constraints" do
    arg = RemiLib::Args::MultiStringArgument.new("str", 'i', group: "Group", help: "Help")
    arg.oneOf = ["foo", "bar"]
    arg << "foo"
    arg.values[0].should eq "foo"
    arg.called?.should be_true

    arg << "bar"
    arg.values[1].should eq "bar"
    arg.called?.should be_true

    expect_raises(RemiLib::Args::ArgumentError) do
      arg << "baz"
    end

    arg.values.size.should eq 2
    arg.values[0].should eq "foo"
    arg.values[1].should eq "bar"
    arg.called?.should be_true

    expect_raises(RemiLib::Args::ArgumentError) do
      arg.values = ["baz", "lol"]
    end

    arg.values.size.should eq 2
    arg.values[0].should eq "foo"
    arg.values[1].should eq "bar"
    arg.called?.should be_true

    arg.setValues!(["baz", "lol"])
    arg.values.size.should eq 2
    arg.times.should eq 2
    arg.values[0].should eq "baz"
    arg.values[1].should eq "lol"
  end

  it "IntArgument sets constraints" do
    arg = RemiLib::Args::IntArgument.new("str", 'i', group: "Group", help: "Help")
    arg.minimum = -9
    arg.maximum = 9
    arg.value = 3
    arg.value.should eq 3
    arg.called?.should be_true

    expect_raises(RemiLib::Args::ArgumentError) do
      arg.value = 69
    end
    arg.value.should eq 3
    arg.called?.should be_true

    expect_raises(RemiLib::Args::ArgumentError) do
      arg.value = -69
    end
    arg.value.should eq 3
    arg.called?.should be_true

    arg.setValue!(69)
    arg.value.should eq 69

    arg.setValue!(-69)
    arg.value.should eq -69
  end

  it "FloatArgument sets constraints" do
    arg = RemiLib::Args::FloatArgument.new("str", 'i', group: "Group", help: "Help")
    arg.minimum = -9.0
    arg.maximum = 9.0
    arg.value = 3.0
    arg.value.should eq 3.0
    arg.called?.should be_true

    expect_raises(RemiLib::Args::ArgumentError) do
      arg.value = 69.0
    end
    arg.value.should eq 3.0
    arg.called?.should be_true

    expect_raises(RemiLib::Args::ArgumentError) do
      arg.value = -69.0
    end
    arg.value.should eq 3.0
    arg.called?.should be_true

    arg.setValue!(69.0)
    arg.value.should eq 69.0

    arg.setValue!(-69.0)
    arg.value.should eq -69.0
  end


  it "MultiIntArgument sets constraints" do
    arg = RemiLib::Args::MultiIntArgument.new("str", 'i', group: "Group", help: "Help")
    arg.minimum = -9
    arg.maximum = 9
    arg << 9
    arg.values[0].should eq 9
    arg.called?.should be_true

    expect_raises(RemiLib::Args::ArgumentError) do
      arg << 69
    end
    arg.values.size.should eq 1
    arg.values[0].should eq 9
    arg.called?.should be_true

    expect_raises(RemiLib::Args::ArgumentError) do
      arg << -69
    end
    arg.values.size.should eq 1
    arg.values[0].should eq 9
    arg.called?.should be_true

    expect_raises(RemiLib::Args::ArgumentError) do
      arg.values = [-69i64]
    end
    arg.values.size.should eq 1
    arg.values[0].should eq 9
    arg.called?.should be_true

    arg.setValues!([69i64])
    arg.values.size.should eq 1
    arg.values[0].should eq 69

    arg.setValues!([-69i64])
    arg.values.size.should eq 1
    arg.values[0].should eq -69
    arg.called?.should be_true
  end
end

describe "Argument parsing" do
  it "Creates a parser" do
    name = "myprog"
    version = "36.69.42"
    parser = RemiLib::Args::ArgParser.new(name, version)
    parser.progName.should eq name
    parser.progVersion.should eq version
    parser.args.size.should eq 2
    parser.args["--help"]?.should_not be_nil
    parser.args["--version"]?.should_not be_nil
  end

  it "Adds arguments" do
    parser = makeParser
    parser.addFlag("test", 't')
    parser.args.size.should eq 3
    parser.args["--test"]?.should_not be_nil
    parser["--test"].called?.should be_false

    parser.addFlag("--test2", 'T')
    parser.args.size.should eq 4
    parser.args["--test2"]?.should_not be_nil
    parser["test2"].called?.should be_false

    expect_raises(Exception, "Duplicate argument: '--test2'") do
      parser.addFlag("--test2", 'f')
    end

    expect_raises(Exception, "Duplicate argument (short name): '--test3' ('-t')") do
      parser.addFlag("--test3", 't')
    end
  end

  it "Handles double-dash positional arguments" do
    parser = makeParser
    parser.addFlag("test", 't')

    expect_raises(RemiLib::Args::ArgumentError, "Invalid argument: --") do
      parser.parse(["fakebin", "-t", "--", "foo", "bar", "baz", "-t"])
    end

    parser = makeParser
    parser.addFlag("test", 't')
    parser.doubleDashPositional = true
    parser.parse(["-t", "--", "foo", "bar", "baz", "-t"])
    parser.positionalArgs.should eq ["foo", "bar", "baz", "-t"]
    parser["test"].called?.should be_true
  end

  it "Handles single-dash arguments" do
    parser = makeParser
    parser.addString("test")
    parser.parse(["--test", "-", "positional"])
    parser["test"].str.should eq "-"
    parser.positionalArgs.should eq ["positional"]

    parser = makeParser
    parser.addString("test")
    expect_raises(RemiLib::Args::ArgumentError, "Invalid argument: -") do
      parser.parse(["--test", "-", "foo", "-"])
    end

    parser = makeParser
    parser.addString("test")
    parser.singleDashPositional = true
    parser.parse(["--test", "-", "foo", "-"])
    parser["test"].str.should eq "-"
    parser.positionalArgs.should eq ["foo", "-"]
  end
end
