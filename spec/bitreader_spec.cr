require "./spec_helper"

describe "BitReader" do
  it "Reads bits correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11111111)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.byte.should eq 0b11010010
    stream.bitpos.should eq 0
    stream.read(8).should eq 0b11010010
    stream.bitpos.should eq 0

    stream.read(3).should eq 0b001
    stream.bitpos.should eq 3
    stream.read(5).should eq 0b01101
    stream.bitpos.should eq 0

    stream.read(1).should eq 0b1
    stream.bitpos.should eq 1
    stream.read(1).should eq 0b1
    stream.bitpos.should eq 2
    stream.read(6).should eq 0b111111
  end

  it "Reads bits across bytes correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11111111)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.read(6).should eq 0b110100
    stream.read(10).should eq 0b1000101101_i64
    stream.read(4).should eq 0b1111
  end

  it "Sets the working byte correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11111111)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.read(3).should eq 0b110
    stream.bitpos.should eq 3
    stream.byte = 0b00011010
    stream.read(5).should eq 0b11010
  end

  it "Counts zeros correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.byte.should eq 0b11010010
    stream.bitpos.should eq 0
    stream.read(8).should eq 0b11010010
    stream.bitpos.should eq 0

    stream.countZeros.should eq 2
    stream.bitpos.should eq 2
    stream.read(6).should eq 0b101101

    io = IO::Memory.new
    io.write_byte(0b00000000)
    io.write_byte(0b00101101)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.countZeros.should eq 10
    stream.bitpos.should eq 2
  end

  it "Counts zeros correctly, discarding the first 1-bit" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.byte.should eq 0b11010010
    stream.bitpos.should eq 0
    stream.read(8).should eq 0b11010010
    stream.bitpos.should eq 0

    stream.countZeros(discardFirstOne: true).should eq 2
    stream.bitpos.should eq 3
    stream.read(5).should eq 0b01101

    io = IO::Memory.new
    io.write_byte(0b00000000)
    io.write_byte(0b00101101)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.countZeros(discardFirstOne: true).should eq 10
    stream.bitpos.should eq 3
  end

  it "Peeks correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.read(1).should eq 1
    stream.bitpos.should eq 1

    stream.peek(1).should eq 1
    stream.bitpos.should eq 1

    stream.peek(3).should eq 0b101
    stream.bitpos.should eq 1

    stream.peek(7).should eq 0b1010010
    stream.bitpos.should eq 1

    stream.peek(10).should eq 0b1010010001
    stream.bitpos.should eq 1

    stream.read(10).should eq 0b1010010001
    stream.bitpos.should eq 3
  end

  it "Reads into Bytes correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11110000)
    io.write_byte(0b00001111)
    io.write_byte(0b01111001)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    buf = Bytes.new(3)

    stream.read(buf).should eq 3
    buf[0].should eq 0b11010010
    buf[1].should eq 0b00101101
    buf[2].should eq 0b11110000

    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11110000)
    io.write_byte(0b00001111)
    io.write_byte(0b01111001)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    buf = Bytes.new(6)

    stream.read(buf, 2, 3).should eq 3
    buf[0].should eq 0
    buf[1].should eq 0
    buf[2].should eq 0b11010010
    buf[3].should eq 0b00101101
    buf[4].should eq 0b11110000
    buf[5].should eq 0
  end

  it "Reads into Arrays correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11110000)
    io.write_byte(0b00001111)
    io.write_byte(0b01111001)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    buf = Array(UInt8).new(3, 0)

    stream.read(buf).should eq 3
    buf[0].should eq 0b11010010
    buf[1].should eq 0b00101101
    buf[2].should eq 0b11110000

    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11110000)
    io.write_byte(0b00001111)
    io.write_byte(0b01111001)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    buf = Array(UInt8).new(6, 0)

    stream.read(buf, 2, 3).should eq 3
    buf[0].should eq 0
    buf[1].should eq 0
    buf[2].should eq 0b11010010
    buf[3].should eq 0b00101101
    buf[4].should eq 0b11110000
    buf[5].should eq 0
  end

  it "Raises when attempting to read into Bytes on a non-byte boundary" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11110000)
    io.write_byte(0b00001111)
    io.write_byte(0b01111001)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    buf = Bytes.new(6)

    stream.read(1)
    expect_raises(RemiLib::BitReader::NotOnByteError, "BitReader is not on a byte boundary") do
      stream.read(buf)
    end
  end

  it "Reads new Bytes correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11110000)
    io.write_byte(0b00001111)
    io.write_byte(0b01111001)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    buf = stream.readBytes(3)
    buf.size.should eq 3
    buf[0].should eq 0b11010010
    buf[1].should eq 0b00101101
    buf[2].should eq 0b11110000

    io.rewind
    stream = RemiLib::BitReader.new(io)
    stream.read(1).should eq 1
    buf = stream.readBytes(2)
    buf.size.should eq 2
    buf[0].should eq 0b10100100
    buf[1].should eq 0b01011011
  end

  it "Gets the byte position correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11110000)
    io.write_byte(0b00001111)
    io.write_byte(0b01111001)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.readBytes(3)
    stream.read(1)
    stream.pos.should eq 3
    stream.bitpos.should eq 1
  end

  it "Advances to the next byte correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11111111)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.pos.should eq 0
    stream.bitpos.should eq 0
    stream.advanceToNextByte
    stream.pos.should eq 0
    stream.bitpos.should eq 0
    stream.peek(8).should eq 0b11010010

    stream.read(3).should eq 0b110
    stream.pos.should eq 0
    stream.bitpos.should eq 3
    stream.advanceToNextByte
    stream.pos.should eq 1
    stream.bitpos.should eq 0
    stream.peek(8).should eq 0b00101101
  end

  it "Reads strings correctly" do
    io = IO::Memory.new
    io << "Test string"
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.readString(4).should eq "Test"
    stream.readString(20).should eq " string"
  end

  it "Rewinds correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11111111)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.pos.should eq 0
    stream.bitpos.should eq 0
    stream.advanceToNextByte
    stream.pos.should eq 0
    stream.bitpos.should eq 0
    stream.peek(8).should eq 0b11010010

    stream.rewind
    stream.pos.should eq 0
    stream.bitpos.should eq 0
    stream.advanceToNextByte
    stream.pos.should eq 0
    stream.bitpos.should eq 0
    stream.peek(8).should eq 0b11010010
  end

  it "Sets position correctly" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11111111)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.pos = 1
    stream.pos.should eq 1
    stream.bitpos.should eq 0
    stream.peek(8).should eq 0b00101101
  end

  it "Can be reinitialized" do
    io = IO::Memory.new
    io.write_byte(0b11010010)
    io.write_byte(0b00101101)
    io.write_byte(0b11111111)
    io.rewind

    stream = RemiLib::BitReader.new(io)
    stream.byte.should eq 0b11010010
    stream.bitpos.should eq 0
    stream.read(8).should eq 0b11010010
    stream.bitpos.should eq 0

    stream.read(3).should eq 0b001
    stream.bitpos.should eq 3
    stream.read(5).should eq 0b01101
    stream.bitpos.should eq 0

    stream.read(1).should eq 0b1
    stream.bitpos.should eq 1
    stream.read(1).should eq 0b1
    stream.bitpos.should eq 2
    stream.read(6).should eq 0b111111

    # Now do the reinitialize
    io2 = IO::Memory.new
    io2.write_byte(0b11111111)
    io2.write_byte(0b11010010)
    io2.write_byte(0b00101101)
    io2.rewind

    stream.reinitialize(io2)
    stream.byte.should eq 0b11111111
    stream.bitpos.should eq 0
    stream.read(8).should eq 0b11111111
    stream.bitpos.should eq 0

    stream.read(3).should eq 0b110
    stream.bitpos.should eq 3
    stream.read(5).should eq 0b10010
    stream.bitpos.should eq 0

    stream.read(1).should eq 0b0
    stream.bitpos.should eq 1
    stream.read(1).should eq 0b0
    stream.bitpos.should eq 2
    stream.read(6).should eq 0b101101
  end
end
