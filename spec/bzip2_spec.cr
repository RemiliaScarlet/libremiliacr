require "./spec_helper"

describe "BZip2" do
  it "Compresses and Decompresses random data (small)" do
    (1..9).each do |level|
      rnd = Random.new
      data = Slice(UInt8).new(16) { |_| rnd.rand(UInt8) }
      destIO = IO::Memory.new

      RemiLib::Compression::BZip2::Writer.open(destIO, level) do |comp|
        comp.write(data)
      end

      destIO.rewind
      RemiLib::Compression::BZip2::Reader.open(destIO) do |decomp|
        decomp.getb_to_end.should eq data
      end
    end
  end

  it "Compresses and Decompresses random data (medium)" do
    (1..9).each do |level|
      rnd = Random.new
      data = Slice(UInt8).new(921600) { |_| rnd.rand(UInt8) }
      destIO = IO::Memory.new

      RemiLib::Compression::BZip2::Writer.open(destIO, level) do |comp|
        comp.write(data)
      end

      destIO.rewind
      RemiLib::Compression::BZip2::Reader.open(destIO) do |decomp|
        decomp.getb_to_end.should eq data
      end
    end
  end

  it "Compresses and Decompresses random data (large)" do
    (1..9).each do |level|
      rnd = Random.new
      data = Slice(UInt8).new(921600 * 2) { |_| rnd.rand(UInt8) }
      destIO = IO::Memory.new

      RemiLib::Compression::BZip2::Writer.open(destIO, level) do |comp|
        comp.write(data)
      end

      destIO.rewind
      RemiLib::Compression::BZip2::Reader.open(destIO) do |decomp|
        decomp.getb_to_end.should eq data
      end
    end
  end
end
