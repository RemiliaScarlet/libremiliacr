require "file_utils"
require "../spec_helper"

module RemiLib::Config
  module Tests
    @[RemiLib::Config::ConfigOpts(filename: "test.json", format: :json)]
    class JsonTest
      include ::RemiLib::Config::ConfigFile
      property field : String = ""
      def initialize
      end
    end

    @[RemiLib::Config::ConfigOpts(filename: "test.yaml", format: :yaml)]
    class YamlTest
      include ::RemiLib::Config::ConfigFile
      property field : String = ""
      def initialize
      end
    end

    def self.getTestResolver : Resolver
      Resolver.custom("", Path["spec", "config"], Path["spec", "config"], dontCreateMissingDirs: true)
    end

    def self.getTestResolverForFile(removeExisting) : Resolver
      if removeExisting
        if Dir.exists?(Path["spec", "config", "test-config"])
          FileUtils.rm_r(Path["spec", "config", "test-config"])
        end
        if Dir.exists?(Path["spec", "config", "test-data"])
          FileUtils.rm_r(Path["spec", "config", "test-data"])
        end
      end

      Resolver.custom("", Path["spec", "config", "test-config"], Path["spec", "config", "test-data"],
                      dontCreateMissingDirs: false)
    end
  end

  describe "Serialization" do
    it "Deserializes data" do
      res = RemiLib::Config::Tests.getTestResolver
      data = RemiLib::Config::Tests::JsonTest.readConfig(res)
      data.field.should eq "Test field"

      data = RemiLib::Config::Tests::YamlTest.readConfig(res)
      data.field.should eq "Test field"
    end

    it "Serializes data" do
      res = RemiLib::Config::Tests.getTestResolver
      data = RemiLib::Config::Tests::JsonTest.readConfig(res)
      str = String.build { |st| data.to_json(st) }
      inStr = IO::Memory.new(str)
      data = RemiLib::Config::Tests::JsonTest.readConfig(inStr)
      data.field.should eq "Test field"

      res = RemiLib::Config::Tests.getTestResolver
      data = RemiLib::Config::Tests::YamlTest.readConfig(res)
      str = String.build { |st| data.to_yaml(st) }
      inStr = IO::Memory.new(str)
      data = RemiLib::Config::Tests::YamlTest.readConfig(inStr)
      data.field.should eq "Test field"
    end

    it "(De)serializes data to a resolved config file" do
      expected = "lol new test field"

      res = RemiLib::Config::Tests.getTestResolverForFile(true)
      data, _ = RemiLib::Config::Tests::JsonTest.readConfig?(res)
      data.field.should be_empty
      data.field = expected
      data.write(res)
      res = RemiLib::Config::Tests.getTestResolverForFile(false)
      data = RemiLib::Config::Tests::JsonTest.readConfig(res)
      data.field.should eq expected

      res = RemiLib::Config::Tests.getTestResolverForFile(true)
      data, _ = RemiLib::Config::Tests::YamlTest.readConfig?(res)
      data.field.should be_empty
      data.field = expected
      data.write(res)
      res = RemiLib::Config::Tests.getTestResolverForFile(false)
      data = RemiLib::Config::Tests::YamlTest.readConfig(res)
      data.field.should eq expected
    end
  end
end
