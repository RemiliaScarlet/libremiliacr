require "file_utils"
require "../spec_helper"

module RemiLib::Config
  describe "Resolver Class" do
    it "Builds directories correctly" do
      {% begin %}
        {% if flag?(:unix) %}
          res = Resolver.xdg("foo", dontCreateMissingDirs: true)
          res.baseName.should eq "foo"

          if ENV["XDG_CONFIG_HOME"]?
            res.configDir.should eq Path[ENV["XDG_CONFIG_HOME"], "foo"]
          else
            res.configDir.should eq Path[ENV["HOME"], ".config", "foo"]
          end

          if ENV["XDG_DATA_HOME"]?
            res.dataDir.should eq Path[ENV["XDG_DATA_HOME"], "foo"]
          else
            res.dataDir.should eq Path[ENV["HOME"], ".local", "share", "foo"]
          end
        {% elsif flag?(:windows) %}
          res = Resolver.windows("foo", dontCreateMissingDirs: true)
          res.baseName.should eq "foo"
          res.configDir.should eq Path[ENV["%APPDATA%"], "foo"]
          res.dataDir.should eq Path[ENV["%APPDATA%"], "foo"]
        {% end %}
      {% end %}

      pwd = Dir.current
      res = Resolver.custom("foo", Path[pwd, "config-dir"], Path[pwd, "data-dir"], dontCreateMissingDirs: true)
      res.baseName.should eq "foo"
      res.configDir.should eq Path[pwd, "config-dir"]
      res.dataDir.should eq Path[pwd, "data-dir"]
    end

    it "Handles empty base names correctly" do
      {% begin %}
        {% if flag?(:unix) %}
          expect_raises(Exception, "Resolver base names cannot be empty") do
            res = Resolver.xdg("", dontCreateMissingDirs: true)
          end
        {% elsif flags?(:windows) %}
          expect_raises(Exception, "Resolver base names cannot be empty") do
            res = Resolver.xdg("", dontCreateMissingDirs: true)
          end
        {% end %}
      {% end %}

      begin
        res = Resolver.custom("", "", "", dontCreateMissingDirs: true)
      rescue Exception
        fail("Custom Resolver should accept empty base names")
      end
    end

    it "Defines data files" do
      pwd = Dir.current
      dataDir = Path[pwd, "data-dir"]
      res = Resolver.custom("", Path[pwd, "config-dir"], dataDir, dontCreateMissingDirs: true)
      res.defineDataFile(:test, "test.dat")

      expect_raises(KeyError, "Duplicate data file: test") do
        res.defineDataFile(:test, "test.dat")
      end

      res.defineDataFile!(:test, "test2.dat")
      res.dataFile(:test).should eq dataDir.join("test2.dat")
    end

    it "Retrieves data file pathnames" do
      pwd = Dir.current
      dataDir = Path[pwd, "data-dir"]
      res = Resolver.custom("", Path[pwd, "config-dir"], dataDir, dontCreateMissingDirs: true)
      res.defineDataFile(:test, "test.dat")
      res.dataFile(:test).should eq dataDir.join("test.dat")

      expect_raises(UnknownDataFileError, "Data file is not registered: fail") do
        res.dataFile(:fail)
      end

      res.dataFile?(:fail).should be_nil
    end

    it "Creates missing directories" do
      pwd = Dir.current
      configDir = Path[pwd, "spec", "test-config-dir"]
      dataDir = Path[pwd, "spec", "test-data-dir"]

      FileUtils.rm_r(configDir) if Dir.exists?(configDir)
      FileUtils.rm_r(dataDir) if Dir.exists?(dataDir)
      Dir.exists?(configDir).should be_falsey
      Dir.exists?(dataDir).should be_falsey

      res = Resolver.custom("", configDir, dataDir, dontCreateMissingDirs: false)
      Dir.exists?(configDir).should be_truthy
      Dir.exists?(dataDir).should be_truthy
    end
  end
end
