require "./spec_helper"

describe "Digest" do
  it "CRC64 with ECMA polynomial computes" do
    ecmaTable = {
      "Hello, world!" => 0x8E59E143665877C4_u64,
      "Crystal" => 0x6297AE23EB504638_u64,
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" => 0x8ED7EFB6C22BE2D6_u64
    }

    isoTable = {
      "Hello, world!" => 0xD176C45C2611D9C2_u64,
      "Crystal" => 0x3EE4087C628DF140_u64,
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" => 0x37F1F22DFB885401_u64
    }

    crc = RemiLib::Digest::Crc64.new
    crc.polynomial.should eq RemiLib::Digest::Crc64::ECMA_POLY
    ecmaTable.each do |str, sum|
      str.each_byte { |byte| crc.update(byte) }
      crc.crc.should eq sum
      crc.reset
      crc.crc.should eq 0
    end

    crc.polynomial = RemiLib::Digest::Crc64::ISO_POLY
    crc.polynomial.should eq RemiLib::Digest::Crc64::ISO_POLY
    isoTable.each do |str, sum|
      str.each_byte { |byte| crc.update(byte) }
      crc.crc.should eq sum
      crc.reset
      crc.crc.should eq 0
    end
  end
end
