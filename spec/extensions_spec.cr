require "./spec_helper"

describe "Standard Library Extensions" do
  it "bitflag? works" do
    field : UInt8 = 0b00000000_u8
    bitflag?(field, 0b00000000).should be_false
    bitflag?(field, 0b00000001).should be_false
    bitflag?(field, 0b00000010).should be_false

    field = 0b00000010_u8
    bitflag?(field, 0b00000000).should be_false
    bitflag?(field, 0b00000001).should be_false
    bitflag?(field, 0b00000010).should be_true
  end

  it "Array#toSlice works" do
    arr : Array(Int32) = [1, 2, 3]
    slc = arr.toSlice

    slc.is_a?(Slice(Int32)).should be_true
    slc.size.should eq 3
    slc[0].should eq 1
    slc[1].should eq 2
    slc[2].should eq 3
  end

  it "Slice#toArray works" do
    arr : Array(Int32) = [1, 2, 3]
    slc = arr.toSlice

    newArr = slc.toArray
    newArr.is_a?(Array(Int32)).should be_true
    newArr.size.should eq 3
    newArr[0].should eq 1
    newArr[1].should eq 2
    newArr[2].should eq 3
  end

  it "IO#withExcursion works" do
    strm = IO::Memory.new
    10.times do |i|
      strm << (i + 65).chr
    end
    strm.rewind

    strm.pos.should eq 0
    strm.withExcursion(5) do
      strm.pos.should eq 5
      strm.read_byte.should eq 70
    end
    strm.pos.should eq 0
  end





  it "IO#readBytes works" do
    strm = IO::Memory.new
    10.times do |i|
      strm.write_byte(i.to_u8!)
    end
    strm.rewind

    result = strm.readBytes(5)
    result.size.should eq 5
    strm.pos.should eq 5
    result[0].should eq 0
    result[1].should eq 1
    result[2].should eq 2
    result[3].should eq 3
    result[4].should eq 4
  end





  it "IO#readInt8 works" do
    strm = IO::Memory.new
    strm.write_byte(0xF1_u8)
    strm.rewind
    strm.readInt8.should eq -15_i8
    strm.pos.should eq 1
  end

  it "IO#writeInt8 works" do
    strm = IO::Memory.new
    strm.writeInt8(-15_i8)
    strm.pos.should eq 1
    strm.rewind
    strm.to_slice.should eq Bytes[0xF1_u8]
  end

  it "IO#readUInt8 works" do
    strm = IO::Memory.new
    strm.write_byte(0xF1_u8)
    strm.rewind
    strm.readUInt8.should eq 0xF1_u8
    strm.pos.should eq 1
  end

  it "IO#writeUInt8 works" do
    strm = IO::Memory.new
    strm.writeUInt8(0xF1_u8)
    strm.pos.should eq 1
    strm.rewind
    strm.to_slice.should eq Bytes[0xF1_u8]
  end





  it "IO#readInt16 works" do
    strm = IO::Memory.new
    strm.write(Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8])
    strm.rewind
    strm.readInt16.should eq 8208
    strm.pos.should eq 2

    strm.rewind
    strm.readInt16BE.should eq 4128
    strm.pos.should eq 2
  end

  it "IO#writeInt16 works" do
    strm = IO::Memory.new
    strm.writeInt16(8208_i16)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8]
    strm.pos.should eq 2

    strm = IO::Memory.new
    strm.writeInt16BE(4128_i16)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8]
    strm.pos.should eq 2
  end

  it "IO#readUInt16 works" do
    strm = IO::Memory.new
    strm.write(Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8])
    strm.rewind
    strm.readUInt16.should eq 8208
    strm.pos.should eq 2

    strm.rewind
    strm.readUInt16BE.should eq 4128
    strm.pos.should eq 2
  end

  it "IO#writeUInt16 works" do
    strm = IO::Memory.new
    strm.writeUInt16(8208_u16)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8]
    strm.pos.should eq 2

    strm = IO::Memory.new
    strm.writeUInt16BE(4128_u16)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8]
    strm.pos.should eq 2
  end





  it "IO#readInt24 works" do
    strm = IO::Memory.new
    strm.write_byte(0x10_u8)
    strm.write_byte(0x20_u8)
    strm.write_byte(0x30_u8)
    strm.write_byte(0x40_u8)
    strm.rewind
    strm.readInt24.should eq 3153936
    strm.pos.should eq 3

    strm.rewind
    strm.readInt24BE.should eq 1056816
    strm.pos.should eq 3
  end

  it "IO#writeInt24 works" do
    strm = IO::Memory.new
    strm.writeInt24(3153936)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8, 0x30_u8]
    strm.pos.should eq 3

    strm = IO::Memory.new
    strm.writeInt24BE(1056816)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8, 0x30_u8]
    strm.pos.should eq 3
  end





  it "IO#readInt32 works" do
    strm = IO::Memory.new
    strm.write(Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8])
    strm.rewind
    strm.readInt32.should eq 1076895760
    strm.pos.should eq 4

    strm.rewind
    strm.readInt32BE.should eq 270544960
    strm.pos.should eq 4
  end

  it "IO#writeInt32 works" do
    strm = IO::Memory.new
    strm.writeInt32(1076895760)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8]
    strm.pos.should eq 4

    strm = IO::Memory.new
    strm.writeInt32BE(270544960)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8]
    strm.pos.should eq 4
  end

  it "IO#readUInt32 works" do
    strm = IO::Memory.new
    strm.write(Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8])
    strm.rewind
    strm.readUInt32.should eq 1076895760
    strm.pos.should eq 4

    strm.rewind
    strm.readUInt32BE.should eq 270544960
    strm.pos.should eq 4
  end

  it "IO#writeUInt32 works" do
    strm = IO::Memory.new
    strm.writeUInt32(1076895760)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8]
    strm.pos.should eq 4

    strm = IO::Memory.new
    strm.writeUInt32BE(270544960)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8]
    strm.pos.should eq 4
  end





  it "IO#readInt64 works" do
    strm = IO::Memory.new
    strm.write(Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8, 0x50_u8, 0x60_u8, 0x70_u8, 0x80_u8])
    strm.rewind
    strm.readInt64.should eq -9191740941672636400_i64
    strm.pos.should eq 8

    strm.rewind
    strm.readInt64BE.should eq 1161981756646125696_i64
    strm.pos.should eq 8
  end

  it "IO#writeInt64 works" do
    strm = IO::Memory.new
    strm.writeInt64(-9191740941672636400_i64)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8, 0x50_u8, 0x60_u8, 0x70_u8, 0x80_u8]
    strm.pos.should eq 8

    strm = IO::Memory.new
    strm.writeInt64BE(1161981756646125696_i64)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8, 0x50_u8, 0x60_u8, 0x70_u8, 0x80_u8]
    strm.pos.should eq 8
  end

  it "IO#readUInt64 works" do
    strm = IO::Memory.new
    strm.write(Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8, 0x50_u8, 0x60_u8, 0x70_u8, 0x80_u8])
    strm.rewind
    strm.readUInt64.should eq 9255003132036915216_u64
    strm.pos.should eq 8

    strm.rewind
    strm.readUInt64BE.should eq 1161981756646125696_u64
    strm.pos.should eq 8
  end

  it "IO#writeUInt64 works" do
    strm = IO::Memory.new
    strm.writeUInt64(9255003132036915216_u64)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8, 0x50_u8, 0x60_u8, 0x70_u8, 0x80_u8]
    strm.pos.should eq 8

    strm = IO::Memory.new
    strm.writeUInt64BE(1161981756646125696_u64)
    strm.to_slice.should eq Bytes[0x10_u8, 0x20_u8, 0x30_u8, 0x40_u8, 0x50_u8, 0x60_u8, 0x70_u8, 0x80_u8]
    strm.pos.should eq 8
  end





  it "Int#prettySize prints correctly" do
    num = 694236

    num.prettySize(alwaysShowAsBytes: true).should eq "694,236 Bytes"
    num.prettySize(alwaysShowAsBytes: true, delimiter: '+').should eq "694+236 Bytes"
    num.prettySize(alwaysShowAsBytes: true, padding: 3).should eq "   694,236 Bytes"
    num.prettySize(alwaysShowAsBytes: true, padding: -5).should eq "694,236 Bytes     "
    num.prettySize(alwaysShowAsBytes: true, padding: 3, padChar: 'x').should eq "xxx694,236 Bytes"

    num.prettySize().should eq "677.96 KiB"
    num.prettySize(decimalPlaces: 3).should eq "677.965 KiB"
    num.prettySize(decimalPlaces: 3, separator: '+').should eq "677+965 KiB"

    num = -694236

    num.prettySize(alwaysShowAsBytes: true).should eq "-694,236 Bytes"
    num.prettySize(alwaysShowAsBytes: true, delimiter: '+').should eq "-694+236 Bytes"
    num.prettySize(alwaysShowAsBytes: true, padding: 3).should eq "   -694,236 Bytes"
    num.prettySize(alwaysShowAsBytes: true, padding: -5).should eq "-694,236 Bytes     "
    num.prettySize(alwaysShowAsBytes: true, padding: 3, padChar: 'x').should eq "xxx-694,236 Bytes"

    num.prettySize().should eq "-677.96 KiB"
    num.prettySize(decimalPlaces: 3).should eq "-677.965 KiB"
    num.prettySize(decimalPlaces: 3, separator: '+').should eq "-677+965 KiB"

    1180591620717411303424u128.prettySize.should eq "1.00 ZiB"
  end

  it "Int#prettySize prints correctly (short suffixes)" do
    num = 694236

    num.prettySize(alwaysShowAsBytes: true, shortSuffix: true).should eq "694,236 B"
    num.prettySize(alwaysShowAsBytes: true, shortSuffix: true, delimiter: '+').should eq "694+236 B"
    num.prettySize(alwaysShowAsBytes: true, shortSuffix: true, padding: 3).should eq "   694,236 B"
    num.prettySize(alwaysShowAsBytes: true, shortSuffix: true, padding: -5).should eq "694,236 B     "
    num.prettySize(alwaysShowAsBytes: true, shortSuffix: true, padding: 3, padChar: 'x').should eq "xxx694,236 B"

    num.prettySize(shortSuffix: true).should eq "677.96 K"
    num.prettySize(decimalPlaces: 3, shortSuffix: true).should eq "677.965 K"
    num.prettySize(decimalPlaces: 3, shortSuffix: true, separator: '+').should eq "677+965 K"

    num = -694236

    num.prettySize(alwaysShowAsBytes: true, shortSuffix: true).should eq "-694,236 B"
    num.prettySize(alwaysShowAsBytes: true, shortSuffix: true, delimiter: '+').should eq "-694+236 B"
    num.prettySize(alwaysShowAsBytes: true, shortSuffix: true, padding: 3).should eq "   -694,236 B"
    num.prettySize(alwaysShowAsBytes: true, shortSuffix: true, padding: -5).should eq "-694,236 B     "
    num.prettySize(alwaysShowAsBytes: true, shortSuffix: true, padding: 3, padChar: 'x').should eq "xxx-694,236 B"

    num.prettySize(shortSuffix: true).should eq "-677.96 K"
    num.prettySize(decimalPlaces: 3, shortSuffix: true).should eq "-677.965 K"
    num.prettySize(decimalPlaces: 3, shortSuffix: true, separator: '+').should eq "-677+965 K"

    1180591620717411303424u128.prettySize(shortSuffix: true).should eq "1.00 Z"
  end

  it "Converts numbers to spoken numbers" do
    num = 10
    Int.positiveSpokenNumber(num).should eq "ten"
    num.positiveSpokenNumber.should eq "ten"

    nums = (0...10).to_a
    numNames = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

    nums.size.should eq numNames.size
    nums.size.times do |i|
      nums[i].positiveSpokenNumber.should eq numNames[i]
    end

    nums = (10..19).to_a
    numNames = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
                "sixteen", "seventeen", "eighteen", "nineteen"]

    nums = [20, 30, 40, 50, 60, 70, 80, 90]
    numNames = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

    nums.size.should eq numNames.size
    nums.size.times do |i|
      nums[i].positiveSpokenNumber.should eq numNames[i]
    end

    27.positiveSpokenNumber.should eq "twenty-seven"
    142.positiveSpokenNumber.should eq "one hundred forty-two"
    9876.positiveSpokenNumber.should eq "nine thousand, eight hundred seventy-six"
    12345.positiveSpokenNumber.should eq "twelve thousand, three hundred forty-five"
    987654.positiveSpokenNumber.should eq "nine hundred eighty-seven thousand, six hundred fifty-four"
    4567891.positiveSpokenNumber.should eq "four million, five hundred sixty-seven thousand, eight hundred ninety-one"
  end

  it "Checks for bits using bitflag?" do
    num : UInt8 = 0b00101101_u8

    ::bitflag?(num, 0b00100000).should be_true
    ::bitflag?(num, 0b00100001).should be_true
    ::bitflag?(num, 0b00101101).should be_true
    ::bitflag?(num, 0b10000000).should be_false
    ::bitflag?(num, 0b01000000).should be_false
  end

  it "Converts to/from universal time" do
    ut : Int64 = 3905278721_i64
    expected : Time = Time.utc(2023, 10, 2, 23, 38, 41)

    expected.toUniversal.should eq ut
    Time.universalToUnix(ut).should eq expected.to_unix
    Time.unixToUniversal(expected.to_unix).should eq ut
    Time.universal(ut).should eq expected
  end

  it "Prints Roman numerals" do
    nums = [ 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000,
             2, 3, 6, 7, 11, 30, 60, 80, 269, 327, 604, 909, 1983 ]
    words = [ "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM",
              "M", "II", "III", "VI", "VII", "XI", "XXX", "LX", "LXXX",
              "CCLXIX", "CCCXXVII", "DCIV", "CMIX", "MCMLXXXIII" ]

    nums.each_with_index do |num, idx|
      num.toRoman.should eq words[idx]
    end

    3999.toRoman?.should eq "MMMCMXCIX"
    4000.toRoman?.should be_nil
    expect_raises(ArgumentError, "Cannot represent number as a roman numeral: 4000") do
      4000.toRoman
    end
  end

  it "Determines leading zeros and ones" do
    0b00001111_u8.numLeadingZeros.should eq 4
    0b11001111_u8.numLeadingZeros.should eq 0
    0b00001111_u8.numLeadingOnes.should eq 0
    0b11001111_u8.numLeadingOnes.should eq 2
  end

  it "Converts to arbitrary bit sizes" do
    num : UInt16 = 0b0010110111010010_u16
    num.asBitSize(4).should eq 2
    num.asBitSize(7).should eq -46
    num.asBitSize(13).should eq 3538
    num.asBitSize(17).should eq 11730

    num.asBitSize(4, true).should eq 2
    num.asBitSize(7, true).should eq 82
    num.asBitSize(13, true).should eq 3538
    num.asBitSize(17, true).should eq 11730
  end
end
