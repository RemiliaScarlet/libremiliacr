require "./spec_helper"

describe "Logging" do
  it "Creates Loggers with custom headers" do
    hdrDefault = "Default"
    hdrVerbose = "Verb"
    hdrDebug = "Dbg"
    hdrWarn = "Warn"
    hdrError = "Err"
    hdrFatal = "Bad!"

    logger = RemiLib::Logger.new(defaultHeader: hdrDefault,
                                 verboseHeader: hdrVerbose,
                                 debugHeader: hdrDebug,
                                 warnHeader: hdrWarn,
                                 errorHeader: hdrError,
                                 fatalHeader: hdrFatal)

    logger.defaultHeader.should eq hdrDefault
    logger.verboseHeader.should eq hdrVerbose
    logger.debugHeader.should eq hdrDebug
    logger.warnHeader.should eq hdrWarn
    logger.errorHeader.should eq hdrError
    logger.fatalHeader.should eq hdrFatal
  end

  it "Sanitizes output" do
    logger = RemiLib::Logger.new(ensureNewline: true)
    logger.defaultStream = IO::Memory.new
    logger.sanitizeOutput = true

    msg = "Test"
    logger.log(msg)
    logger.defaultStream.to_s.should eq "#{msg}\n"
    logger.defaultStream.as(IO::Memory).clear

    logger.log("#{msg}\n")
    logger.defaultStream.to_s.should eq "#{msg}\n"
    logger.defaultStream.as(IO::Memory).clear

    logger.log("\e#{msg}")
    logger.defaultStream.to_s.should eq "ESC#{msg}\n"
    logger.defaultStream.as(IO::Memory).clear

    logger.log("\e#{msg}\n")
    logger.defaultStream.to_s.should eq "ESC#{msg}\n"
  end

  it "Logs default messages" do
    logger = RemiLib::Logger.new(ensureNewline: true, defaultHeader: "", showHeaders: true)
    logger.defaultStream = IO::Memory.new
    logger.debugStream = IO::Memory.new
    logger.warnStream = IO::Memory.new
    logger.errorStream = IO::Memory.new
    logger.noColors = true

    msg = "Test message"
    hdr = "Header"

    # Log a message
    logger.log(msg)
    logger.defaultStream.to_s.should eq "#{msg}\n"
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty

    # With a different header
    logger.defaultStream.as(IO::Memory).clear
    logger.defaultHeader = hdr
    logger.log(msg)
    logger.defaultStream.to_s.should eq "[#{hdr}]: #{msg}\n"
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty

    # With a different header, but showHeaders is false
    logger.defaultStream.as(IO::Memory).clear
    logger.defaultHeader = hdr
    logger.showHeaders = false
    logger.log(msg)
    logger.defaultStream.to_s.should eq "#{msg}\n"
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty

    # With a timestamp + headers
    logger.defaultStream.as(IO::Memory).clear
    logger.defaultHeader = hdr
    logger.showHeaders = true
    ts = "%a, %b %-d, %R" # Use a timestamp without seconds juuuuuust in case
    logger.timestamp = ts
    time = Time.local
    logger.log(msg)
    logger.defaultStream.to_s.should eq "[#{hdr} - #{time.to_s(ts)}]: #{msg}\n"
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty
  end

  it "Logs debug messages" do
    logger = RemiLib::Logger.new(ensureNewline: true, defaultHeader: "", showHeaders: true)
    logger.defaultStream = IO::Memory.new
    logger.debugStream = IO::Memory.new
    logger.warnStream = IO::Memory.new
    logger.errorStream = IO::Memory.new
    logger.noColors = true

    msg = "Test message"

    logger.debugLevel = 0u8
    logger.dlog(msg)
    logger.defaultStream.to_s.should be_empty
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty
    logger.debugStream.as(IO::Memory).clear

    logger.debugLevel = 3u8
    logger.dlog(3, msg)
    logger.defaultStream.to_s.should be_empty
    logger.debugStream.to_s.should eq "[#{logger.debugHeader}]: #{msg}\n"
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty
    logger.debugStream.as(IO::Memory).clear

    logger.debugLevel = 99
    logger.dlog(3, msg)
    logger.defaultStream.to_s.should be_empty
    logger.debugStream.to_s.should eq "[#{logger.debugHeader}]: #{msg}\n"
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty
    logger.debugStream.as(IO::Memory).clear

    logger.debugLevel = 255
    logger.dlog(msg)
    logger.defaultStream.to_s.should be_empty
    logger.debugStream.to_s.should eq "[#{logger.debugHeader}]: #{msg}\n"
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty
    logger.debugStream.as(IO::Memory).clear

    logger.debugLevel = 255
    logger.dlog(3, msg)
    logger.defaultStream.to_s.should be_empty
    logger.debugStream.to_s.should eq "[#{logger.debugHeader}]: #{msg}\n"
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty
  end

  it "Logs verbose messages" do
    logger = RemiLib::Logger.new(ensureNewline: true, defaultHeader: "", showHeaders: true)
    logger.defaultStream = IO::Memory.new
    logger.debugStream = IO::Memory.new
    logger.warnStream = IO::Memory.new
    logger.errorStream = IO::Memory.new
    logger.noColors = true

    msg = "Test message"

    logger.verbosityLevel = 0u8
    logger.vlog(msg)
    logger.defaultStream.to_s.should be_empty
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty
    logger.defaultStream.as(IO::Memory).clear

    logger.verbosityLevel = 3u8
    logger.vlog(3, msg)
    logger.defaultStream.to_s.should eq "[#{logger.verboseHeader}]: #{msg}\n"
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty
    logger.defaultStream.as(IO::Memory).clear

    logger.verbosityLevel = 99
    logger.vlog(3, msg)
    logger.defaultStream.to_s.should eq "[#{logger.verboseHeader}]: #{msg}\n"
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty
    logger.defaultStream.as(IO::Memory).clear

    logger.verbosityLevel = 255
    logger.vlog(msg)
    logger.defaultStream.to_s.should eq "[#{logger.verboseHeader}]: #{msg}\n"
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty
    logger.defaultStream.as(IO::Memory).clear

    logger.verbosityLevel = 255
    logger.vlog(3, msg)
    logger.defaultStream.to_s.should eq "[#{logger.verboseHeader}]: #{msg}\n"
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should be_empty
  end

  it "Logs warning messages" do
    logger = RemiLib::Logger.new(ensureNewline: true, defaultHeader: "", showHeaders: true)
    logger.defaultStream = IO::Memory.new
    logger.debugStream = IO::Memory.new
    logger.warnStream = IO::Memory.new
    logger.errorStream = IO::Memory.new
    logger.noColors = true

    msg = "Test message"
    logger.warn(msg)
    logger.defaultStream.to_s.should be_empty
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should eq "[#{logger.warnHeader}]: #{msg}\n"
    logger.errorStream.to_s.should be_empty
  end

  it "Logs error messages" do
    logger = RemiLib::Logger.new(ensureNewline: true, defaultHeader: "", showHeaders: true)
    logger.defaultStream = IO::Memory.new
    logger.debugStream = IO::Memory.new
    logger.warnStream = IO::Memory.new
    logger.errorStream = IO::Memory.new
    logger.noColors = true

    msg = "Test message"
    logger.error(msg)
    logger.defaultStream.to_s.should be_empty
    logger.debugStream.to_s.should be_empty
    logger.warnStream.to_s.should be_empty
    logger.errorStream.to_s.should eq "[#{logger.errorHeader}]: #{msg}\n"
  end
end
