require "./spec_helper"

alias MathUnit = RemiMath::Conv::Unit

describe "Math Conversions" do
  it "Converts from inches" do
    inches = 69
    MathUnit::Inches.conv(MathUnit::Inches, inches).should eq inches
    MathUnit::Inches.conv(MathUnit::Feet, inches).should eq BigDecimal.new("5.75")
    MathUnit::Inches.conv(MathUnit::Centimeters, inches).should eq BigDecimal.new("175.26")
    MathUnit::Inches.conv(MathUnit::Millimeters, inches).should eq BigDecimal.new("1752.6")
    MathUnit::Inches.conv(MathUnit::Micrometers, inches).should eq BigDecimal.new("1752600")
    MathUnit::Inches.conv(MathUnit::Nanometers, inches).should eq BigDecimal.new("1752600000")
    MathUnit::Inches.conv(MathUnit::Picometers, inches).should eq BigDecimal.new("1752600000000")
    MathUnit::Inches.conv(MathUnit::Miles, inches)
      .not_nil!.should be_close(BigDecimal.new("0.0010890151515151515152"),
                                BigDecimal.new("0.0000000000000000001"))
    MathUnit::Inches.conv(MathUnit::Kilometers, inches).not_nil!.should be_close(0.0017526, 0.00000001)
    MathUnit::Inches.conv(MathUnit::Parsecs, inches)
      .not_nil!.should be_close(BigDecimal.new("0.00000000000000005679789783069401828"),
                                BigDecimal.new("0.00000000000000000000000000000000001"))
    MathUnit::Inches.conv(MathUnit::Lightyears, inches)
      .not_nil!.should be_close(BigDecimal.new("0.0000000000000001852513461005891606"),
                                BigDecimal.new("0.0000000000000000000000000000000001"))
  end

  it "Converts from miles per hour" do
    val = 69
    MathUnit::MilesPerHour.conv(MathUnit::MilesPerHour, val).should eq val
    MathUnit::MilesPerHour.conv(MathUnit::KilometersPerHour, val)
      .not_nil!.should be_close(BigDecimal.new("111.04473600667192308"),
                                BigDecimal.new("000.00000000000000001"))
    MathUnit::MilesPerHour.conv(MathUnit::Knots, val)
      .not_nil!.should be_close(BigDecimal.new("59.959360692371350035"),
                                BigDecimal.new("00.00000000000000001"))
  end

  it "Converts from kilometers per hour" do
    val = 69
    MathUnit::KilometersPerHour.conv(MathUnit::MilesPerHour, val)
      .not_nil!.should be_close(BigDecimal.new("42.874612264376043905"),
                                BigDecimal.new("00.00000000000000001"))
    MathUnit::KilometersPerHour.conv(MathUnit::KilometersPerHour, val).should eq val
    MathUnit::KilometersPerHour.conv(MathUnit::Knots, val)
      .not_nil!.should be_close(BigDecimal.new("37.257019438444924406"),
                                BigDecimal.new("00.00000000000000001"))
  end

  it "Converts from knots" do
    val = 69
    MathUnit::MilesPerHour.conv(MathUnit::MilesPerHour, val).should eq val
    MathUnit::MilesPerHour.conv(MathUnit::KilometersPerHour, val)
      .not_nil!.should be_close(BigDecimal.new("111.04473600667192308"),
                                BigDecimal.new("000.00000000000000001"))
    MathUnit::MilesPerHour.conv(MathUnit::Knots, val)
      .not_nil!.should be_close(BigDecimal.new("59.959360692371350035"),
                                BigDecimal.new("00.00000000000000001"))
  end

  it "Converts from kilograms" do
    val = 69
    MathUnit::Kilograms.conv(MathUnit::Kilograms, val).should eq val
    MathUnit::Kilograms.conv(MathUnit::Pounds, val).should eq BigDecimal.new("152.1190849938")
  end

  it "Converts from pounds" do
    val = 69
    MathUnit::Pounds.conv(MathUnit::Pounds, val).should eq val
    MathUnit::Pounds.conv(MathUnit::Kilograms, val).should eq BigDecimal.new("31.297848")
  end

  it "Converts from celsius" do
    val = 69
    MathUnit::Celsius.conv(MathUnit::Celsius, val).should eq val
    MathUnit::Celsius.conv(MathUnit::Fahrenheit, val).should eq BigDecimal.new("156.2")
    MathUnit::Celsius.conv(MathUnit::Kelvin, val).should eq BigDecimal.new("342.15")
    MathUnit::Celsius.conv(MathUnit::Rankine, val).should eq BigDecimal.new("615.87")
  end

  it "Converts from fahrenheit" do
    val = 69
    MathUnit::Fahrenheit.conv(MathUnit::Celsius, val).should eq BigDecimal.new("20.5555555555555572")
    MathUnit::Fahrenheit.conv(MathUnit::Fahrenheit, val).should eq val
    MathUnit::Fahrenheit.conv(MathUnit::Kelvin, val).should eq BigDecimal.new("293.7055555555555572")
    MathUnit::Fahrenheit.conv(MathUnit::Rankine, val).should eq BigDecimal.new("528.67")
  end

  it "Converts from kelvin" do
    val = 69
    MathUnit::Kelvin.conv(MathUnit::Celsius, val).should eq BigDecimal.new("-204.15")
    MathUnit::Kelvin.conv(MathUnit::Fahrenheit, val).should eq BigDecimal.new("-335.47")
    MathUnit::Kelvin.conv(MathUnit::Kelvin, val).should eq val
    MathUnit::Kelvin.conv(MathUnit::Rankine, val).should eq BigDecimal.new("124.2")
  end

  it "Converts from rankine" do
    val = 69
    MathUnit::Rankine.conv(MathUnit::Celsius, val).should eq BigDecimal.new("-234.816666666666685452")
    MathUnit::Rankine.conv(MathUnit::Fahrenheit, val).should eq BigDecimal.new("-390.67")
    MathUnit::Rankine.conv(MathUnit::Kelvin, val).should eq BigDecimal.new("38.3333333333333364")
    MathUnit::Rankine.conv(MathUnit::Rankine, val).should eq val
  end

  it "Converts from degrees" do
    val = 69
    MathUnit::Degrees.conv(MathUnit::Degrees, val).should eq val
    MathUnit::Degrees.conv(MathUnit::Radians, val).should eq BigDecimal.new("1.204277183876087355")
  end

  it "Converts from radians" do
    val = 69
    MathUnit::Radians.conv(MathUnit::Degrees, val).should eq BigDecimal.new("3953.40878640268008")
    MathUnit::Radians.conv(MathUnit::Radians, val).should eq val
  end

  it "Converts from liters" do
    val = 69
    MathUnit::Liters.conv(MathUnit::Liters, val).should eq val
    MathUnit::Liters.conv(MathUnit::UsGallons, val).should eq BigDecimal.new("18.22773")
    MathUnit::Liters.conv(MathUnit::UsPints, val).should eq BigDecimal.new("145.8246")
  end

  it "Converts from US gallons" do
    val = 69
    MathUnit::UsGallons.conv(MathUnit::Liters, val)
      .not_nil!.should be_close(BigDecimal.new("261.19544232880342203883862"),
                                BigDecimal.new("0.00000000000000000000001"))
    MathUnit::UsGallons.conv(MathUnit::UsGallons, val).should eq val
    MathUnit::UsGallons.conv(MathUnit::UsPints, val).should eq BigDecimal.new("552")
  end

  it "Converts from US pints" do
    val = 69
    MathUnit::UsPints.conv(MathUnit::Liters, val)
      .not_nil!.should be_close(BigDecimal.new("32.64881234030472224850"),
                                BigDecimal.new("0.00000000000000000001"))
    MathUnit::UsPints.conv(MathUnit::UsGallons, val).should eq BigDecimal.new("8.625")
    MathUnit::UsPints.conv(MathUnit::UsPints, val).should eq val
  end

  it "Parses standard unit names" do
    MathUnit.parseUnit?("Inches").should eq MathUnit::Inches
    MathUnit.parseUnit?("feet").should eq MathUnit::Feet
    MathUnit.parseUnit?("CENTIMETERS").should eq MathUnit::Centimeters
    MathUnit.parseUnit?("MilliMeters").should eq MathUnit::Millimeters
    MathUnit.parseUnit?("Micrometers").should eq MathUnit::Micrometers
    MathUnit.parseUnit?("nanometers").should eq MathUnit::Nanometers
    MathUnit.parseUnit?("picoMeters").should eq MathUnit::Picometers
    MathUnit.parseUnit?("miles").should eq MathUnit::Miles
    MathUnit.parseUnit?("nautical mile").should eq MathUnit::NauticalMiles
    MathUnit.parseUnit?("kilometers").should eq MathUnit::Kilometers
    MathUnit.parseUnit?("parsecs").should eq MathUnit::Parsecs
    MathUnit.parseUnit?("lightyears").should eq MathUnit::Lightyears

    MathUnit.parseUnit?("miles per hour").should eq MathUnit::MilesPerHour
    MathUnit.parseUnit?("kilometers per hour").should eq MathUnit::KilometersPerHour
    MathUnit.parseUnit?("knots").should eq MathUnit::Knots

    MathUnit.parseUnit?("pounds").should eq MathUnit::Pounds
    MathUnit.parseUnit?("kilograms").should eq MathUnit::Kilograms

    MathUnit.parseUnit?("celsius").should eq MathUnit::Celsius
    MathUnit.parseUnit?("fahrenheit").should eq MathUnit::Fahrenheit
    MathUnit.parseUnit?("kelvin").should eq MathUnit::Kelvin
    MathUnit.parseUnit?("rankine").should eq MathUnit::Rankine

    MathUnit.parseUnit?("degrees").should eq MathUnit::Degrees
    MathUnit.parseUnit?("radians").should eq MathUnit::Radians

    MathUnit.parseUnit?("liters").should eq MathUnit::Liters
    MathUnit.parseUnit?("us_gallons").should eq MathUnit::UsGallons
    MathUnit.parseUnit?("UsGallons").should eq MathUnit::UsGallons
    MathUnit.parseUnit?("usgallons").should eq MathUnit::UsGallons
    MathUnit.parseUnit?("us_pints").should eq MathUnit::UsPints
    MathUnit.parseUnit?("uspints").should eq MathUnit::UsPints
    MathUnit.parseUnit?("UsPints").should eq MathUnit::UsPints
  end

  it "Parses unit abbreviations" do
    MathUnit.parseUnit?("in").should eq MathUnit::Inches
    MathUnit.parseUnit?("ft").should eq MathUnit::Feet
    MathUnit.parseUnit?("cm").should eq MathUnit::Centimeters
    MathUnit.parseUnit?("mm").should eq MathUnit::Millimeters
    MathUnit.parseUnit?("µm").should eq MathUnit::Micrometers
    MathUnit.parseUnit?("um").should eq MathUnit::Micrometers # Alternate
    MathUnit.parseUnit?("nm").should eq MathUnit::Nanometers
    MathUnit.parseUnit?("pm").should eq MathUnit::Picometers
    MathUnit.parseUnit?("mi").should eq MathUnit::Miles
    MathUnit.parseUnit?("nmi").should eq MathUnit::NauticalMiles
    MathUnit.parseUnit?("km").should eq MathUnit::Kilometers
    MathUnit.parseUnit?("pc").should eq MathUnit::Parsecs
    MathUnit.parseUnit?("ly").should eq MathUnit::Lightyears

    MathUnit.parseUnit?("mph").should eq MathUnit::MilesPerHour
    MathUnit.parseUnit?("kph").should eq MathUnit::KilometersPerHour
    MathUnit.parseUnit?("kt").should eq MathUnit::Knots

    MathUnit.parseUnit?("lb").should eq MathUnit::Pounds
    MathUnit.parseUnit?("kg").should eq MathUnit::Kilograms

    MathUnit.parseUnit?("c").should eq MathUnit::Celsius
    MathUnit.parseUnit?("f").should eq MathUnit::Fahrenheit
    MathUnit.parseUnit?("k").should eq MathUnit::Kelvin
    MathUnit.parseUnit?("r").should eq MathUnit::Rankine

    MathUnit.parseUnit?("deg").should eq MathUnit::Degrees
    MathUnit.parseUnit?("rad").should eq MathUnit::Radians

    MathUnit.parseUnit?("l").should eq MathUnit::Liters
    MathUnit.parseUnit?("gal").should eq MathUnit::UsGallons
    MathUnit.parseUnit?("pt").should eq MathUnit::UsPints
  end

  it "Parses alternate unit names" do
    MathUnit.parseUnit?("Inch").should eq MathUnit::Inches
    MathUnit.parseUnit?("fOot").should eq MathUnit::Feet
    MathUnit.parseUnit?("centimetres").should eq MathUnit::Centimeters
    MathUnit.parseUnit?("centimetre").should eq MathUnit::Centimeters
    MathUnit.parseUnit?("centimeter").should eq MathUnit::Centimeters

    MathUnit.parseUnit?("millimetres").should eq MathUnit::Millimeters
    MathUnit.parseUnit?("millimetre").should eq MathUnit::Millimeters
    MathUnit.parseUnit?("millimeter").should eq MathUnit::Millimeters

    MathUnit.parseUnit?("micrometres").should eq MathUnit::Micrometers
    MathUnit.parseUnit?("micrometre").should eq MathUnit::Micrometers
    MathUnit.parseUnit?("micrometer").should eq MathUnit::Micrometers
    MathUnit.parseUnit?("microns").should eq MathUnit::Micrometers
    MathUnit.parseUnit?("micron").should eq MathUnit::Micrometers

    MathUnit.parseUnit?("nanometres").should eq MathUnit::Nanometers
    MathUnit.parseUnit?("nanometre").should eq MathUnit::Nanometers
    MathUnit.parseUnit?("nanometer").should eq MathUnit::Nanometers

    MathUnit.parseUnit?("picometres").should eq MathUnit::Picometers
    MathUnit.parseUnit?("picometre").should eq MathUnit::Picometers
    MathUnit.parseUnit?("picometer").should eq MathUnit::Picometers

    MathUnit.parseUnit?("kilometres").should eq MathUnit::Kilometers
    MathUnit.parseUnit?("kilometre").should eq MathUnit::Kilometers
    MathUnit.parseUnit?("kilometer").should eq MathUnit::Kilometers

    MathUnit.parseUnit?("mile").should eq MathUnit::Miles
    MathUnit.parseUnit?("paRSec").should eq MathUnit::Parsecs

    MathUnit.parseUnit?("lightyear").should eq MathUnit::Lightyears
    MathUnit.parseUnit?("light YEAR").should eq MathUnit::Lightyears
    MathUnit.parseUnit?("light Years").should eq MathUnit::Lightyears

    MathUnit.parseUnit?("nautical miles").should eq MathUnit::NauticalMiles
    MathUnit.parseUnit?("kilometres per hour").should eq MathUnit::KilometersPerHour

    MathUnit.parseUnit?("kilogram").should eq MathUnit::Kilograms
    MathUnit.parseUnit?("PounD").should eq MathUnit::Pounds

    MathUnit.parseUnit?("freedom units").should eq MathUnit::Fahrenheit

    MathUnit.parseUnit?("litres").should eq MathUnit::Liters
    MathUnit.parseUnit?("litre").should eq MathUnit::Liters
    MathUnit.parseUnit?("liter").should eq MathUnit::Liters

    MathUnit.parseUnit?("gallons").should eq MathUnit::UsGallons
    MathUnit.parseUnit?("gallon").should eq MathUnit::UsGallons

    MathUnit.parseUnit?("pints").should eq MathUnit::UsPints
    MathUnit.parseUnit?("pint").should eq MathUnit::UsPints
  end

  it "Converts to the abbreviation" do
    MathUnit::Inches.abbrev.should eq "in"
    MathUnit::Feet.abbrev.should eq "ft"
    MathUnit::Centimeters.abbrev.should eq "cm"
    MathUnit::Millimeters.abbrev.should eq "mm"
    MathUnit::Micrometers.abbrev.should eq "µm"
    MathUnit::Nanometers.abbrev.should eq "nm"
    MathUnit::Picometers.abbrev.should eq "pm"
    MathUnit::Miles.abbrev.should eq "mi"
    MathUnit::NauticalMiles.abbrev.should eq "nmi"
    MathUnit::Kilometers.abbrev.should eq "km"
    MathUnit::Parsecs.abbrev.should eq "pc"
    MathUnit::Lightyears.abbrev.should eq "ly"
    MathUnit::MilesPerHour.abbrev.should eq "mph"
    MathUnit::KilometersPerHour.abbrev.should eq "kph"
    MathUnit::Knots.abbrev.should eq "kt"
    MathUnit::Kilograms.abbrev.should eq "kg"
    MathUnit::Pounds.abbrev.should eq "lb"
    MathUnit::Celsius.abbrev.should eq "c"
    MathUnit::Fahrenheit.abbrev.should eq "f"
    MathUnit::Kelvin.abbrev.should eq "k"
    MathUnit::Rankine.abbrev.should eq "r"
    MathUnit::Degrees.abbrev.should eq "deg"
    MathUnit::Radians.abbrev.should eq "rad"
    MathUnit::Liters.abbrev.should eq "l"
    MathUnit::UsGallons.abbrev.should eq "gal"
    MathUnit::UsPints.abbrev.should eq "pt"
  end

  it "No duplicate abbreviations" do
    allAbbrevs = MathUnit.values.map &.abbrev
    allAbbrevs.size.should eq allAbbrevs.uniq.size
  end
end
