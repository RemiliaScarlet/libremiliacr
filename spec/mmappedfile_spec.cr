require "./spec_helper"

describe "MmappedFile" do
  it "Opens and closed correctly" do
    RemiLib::MmappedFile.open("./spec/mmap-data.bin", File.size("./spec/mmap-data.bin")) do |file|
      file.len.should eq 1024
      file.pos.should eq 0
    end
  end

  it "Reads integers" do
    RemiLib::MmappedFile.open("./spec/mmap-data.bin", File.size("./spec/mmap-data.bin")) do |file|
      file.readUInt8.should eq 0x40_u8
      file.readUInt8.should eq 0x61_u8
      file.pos.should eq 2
      file.getUInt8(0x20).should eq 0x86_u8
      file.pos.should eq 2

      file.readUInt16.should eq 0xEA3F_u16
      file.pos.should eq 4
      file.getUInt16(0x50).should eq 0x130C_u16
      file.pos.should eq 4

      file.readUInt24.should eq 0x3D2CE3_u32
      file.pos.should eq 7
      file.getUInt24(0x60).should eq 0xA36FA4_u32
      file.pos.should eq 7

      file.pos = 8
      file.pos.should eq 8

      file.readUInt32.should eq 0x5AD10ADC_u32
      file.pos.should eq 12
      file.getUInt32(0x70).should eq 0x3D84088F_u32
      file.pos.should eq 12

      file.readUInt64.should eq 0xBB417E89351A0D7E_u64
      file.pos.should eq 20
      file.getUInt64(0x80).should eq 0x676CE5338C1462FC_u64
      file.pos.should eq 20

      file.readUInt128.should eq 0x6537408614733D67E6BD7637A6905A52_u128
      file.pos.should eq 36
      file.getUInt128(0xA0).should eq 0xD17D910D0580428A145D9ECF986DDFD7_u128
      file.pos.should eq 36
    end
  end
end
