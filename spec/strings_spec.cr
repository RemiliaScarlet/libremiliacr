require "./spec_helper"
require "math"

describe "strings.cr" do
  it "Trims extra nulls" do
    str : String = "Hello\0\0\0"
    str = RemiLib.trimNulls(str)
    str.size.should eq 5
    str.should eq "Hello"
  end

  it "Trims extra nulls with nulls in the middle" do
    str : String = "He\0llo\0\0\0"
    str = RemiLib.trimNulls(str)
    str.size.should eq 2
    str.should eq "He"

    str = "He\0llo\0\0\0"
    str = RemiLib.trimNulls(str, fromFirst: false)
    str.size.should eq 6
    str.should eq "He\0llo"
  end

  it "Pads strings" do
    str : String = "Hello"
    str = RemiLib.padString(str, 10)
    str.size.should eq 10
    str.should eq "Hello\0\0\0\0\0"

    str = "Hello"
    str = RemiLib.padString(str, 5)
    str.size.should eq 5
    str.should eq "Hello"
  end

  it "Writes padded string" do
    io : IO::Memory = IO::Memory.new
    str = "Hello"
    RemiLib.writePaddedString(str, io, 10)
    data = io.to_slice
    data.size.should eq 10

    "Hello\0\0\0\0\0".bytes.each_with_index do |byte, idx|
      data[idx].should eq byte
    end

    io = IO::Memory.new
    str = "Hello"
    RemiLib.writePaddedString(str, io, 5)
    data = io.to_slice
    data.size.should eq 5
    "Hello".bytes.each_with_index do |byte, idx|
      data[idx].should eq byte
    end
  end

  it "Builds wrapped strings" do
    text : String = "Hello world this is a long line of text"
    io : IO::Memory = IO::Memory.new

    RemiLib.buildWrapped(text, io, maxWidth: 10)
    result : String = io.to_s
    result.should eq %|Hello
world this
is a long
line of
text|
  end

  it "Builds wrapped strings that already fit" do
    text : String = "Hello world this is a long line of text"
    io : IO::Memory = IO::Memory.new

    RemiLib.buildWrapped(text, io, maxWidth: 80)
    result : String = io.to_s
    result.should eq text
  end

  it "Splits as args" do
    line : String = %|foo bar "baz lol" test "-12"|
    args = RemiLib.splitAsArgs(line)
    args.should eq [ "foo", "bar", "\"baz lol\"", "test", "\"-12\"" ]
  end

  it "Splits as args and removes quotes" do
    line : String = %|foo bar "baz lol" test "-12"|
    args = RemiLib.splitAsArgs(line, removeQuotes: true)
    args.should eq [ "foo", "bar", "baz lol", "test", "-12" ]
  end

  it "Sanitizes strings" do
    str = "foo"
    RemiLib.sanitize(str).should eq str
    RemiLib::SANITIZE_CHARS.each do |k, v|
      RemiLib.sanitize("#{k}#{str}").should eq "#{v}#{str}"
    end

    RemiLib::SANITIZE_EXTRA_CHARS.each do |k, v|
      RemiLib.sanitize("#{k}#{str}").should eq "#{v}#{str}"
    end

    RemiLib::SANITIZE_EXTRA_CHARS.each do |k, _|
      RemiLib.sanitize("#{k}#{str}", true).should eq "#{k}#{str}"
    end
  end
end
