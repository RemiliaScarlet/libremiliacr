require "../src/remilib/console/terminology"

if RemiLib::Console::Terminology.runningTerminology?
  RemiLib::Console::Terminology.showMedia("spec/crystal.png", 40, 12)
  puts "^^^ #showMedia ^^^"

  RemiLib::Console::Terminology.showThumb("spec/crystal.png", 10, 3)
  puts "^^^ #showThumb without link ^^^"

  RemiLib::Console::Terminology.showThumb("spec/crystal.png", 10, 3, link: "https://crystal-lang.org/")
  puts "^^^ #showThumb with link to https://crystal-lang.org/ ^^^"

  width, height, charWidth, charHeight = RemiLib::Console::Terminology.queryGrid
  puts "\nQuerying grid/font sizes: #{width} x #{height}, cells are #{charWidth} x #{charHeight} pixels"

  puts "\nPress enter to popup the image, followed by the Crystal website"
  STDIN.gets
  RemiLib::Console::Terminology.popup("spec/crystal.png")

  puts "Press enter to popup the image twice (with enqueuing)"
  STDIN.gets
  RemiLib::Console::Terminology.popup("spec/crystal.png", enqueue: true)
  RemiLib::Console::Terminology.popup("spec/crystal.png", enqueue: true)
else
  puts "Not running in Terminology"
end
