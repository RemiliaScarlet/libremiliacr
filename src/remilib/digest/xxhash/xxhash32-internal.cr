#### MIT License
####
#### Copyright (c) 2023-2024 Remilia Scarlet
#### Copyright (c) 2018 Melnik Alexander
####
#### Permission is hereby granted, free of charge, to any person obtaining a
#### copy of this software and associated documentation files (the "Software"),
#### to deal in the Software without restriction, including without limitation
#### the rights to use, copy, modify, merge, publish, distribute, sublicense,
#### and/or sell copies of the Software, and to permit persons to whom the
#### Software is furnished to do so, subject to the following conditions:
####
#### The above copyright notice and this permission notice shall be included in
#### all copies or substantial portions of the Software.
####
#### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#### IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#### FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#### AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#### LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#### FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#### DEALINGS IN THE SOFTWARE.
require "./xxhash-common"

####
#### Ported from C#:
#### https://github.com/uranium62/xxHash/tree/6b20e7f7b32dfc29e5019d3d35f5b7270f1656f3
####

module RemiLib::Digest
  class XXHash32 < XXHashInternal
    @[AlwaysInline]
    protected def self.xxh32(input : Bytes, seed : UInt32) : UInt32
      inputPtr : PUInt8 = input.to_unsafe
      len = input.size
      h32 : UInt32 = 0

      if len >= 16
        limit : PUInt8 = (inputPtr + input.size) - 15
        v1 : UInt32 = seed &+ XXH_PRIME32_1 &+ XXH_PRIME32_2
        v2 : UInt32 = seed &+ XXH_PRIME32_2
        v3 : UInt32 = seed
        v4 : UInt32 = seed &- XXH_PRIME32_1

        loop do
          {% for i in 1..4 %}
            v{{i}} = round(v{{i}}, inputPtr.unsafe_as(PUInt32).value)
            inputPtr += 4
          {% end %}
          break unless inputPtr < limit
        end

        h32 = rotl32(v1, 1) &+
              rotl32(v2, 7) &+
              rotl32(v3, 12) &+
              rotl32(v4, 18)
      else
        h32 = seed &+ XXH_PRIME32_5
      end

      h32 = h32 &+ len
      xxhFinalize(h32, inputPtr, len)
    end

    @[AlwaysInline]
    protected def self.round(acc : UInt32, input : UInt32) : UInt32
      acc = acc &+ (input &* XXH_PRIME32_2)
      acc = rotl32(acc, 13)
      acc &* XXH_PRIME32_1
    end

    @[AlwaysInline]
    protected def self.avalanch(hash : UInt32) : UInt32
      hash ^= hash.unsafe_shr(15)
      hash = hash &* XXH_PRIME32_2
      hash ^= hash.unsafe_shr(13)
      hash = hash &* XXH_PRIME32_3
      hash ^ hash.unsafe_shr(16)
    end

    @[AlwaysInline]
    protected def self.xxhFinalize(hash : UInt32, ptr : PUInt8, len : Int) : UInt32
      len &= 15
      while len >= 4
        hash = hash &+ (ptr.unsafe_as(PUInt32).value &* XXH_PRIME32_3)
        ptr += 4
        hash = rotl32(hash, 17) &* XXH_PRIME32_4
        len = len &- 4
      end

      while len > 0
        hash = hash &+ (ptr.value.to_u32! &* XXH_PRIME32_5)
        ptr += 1
        hash = rotl32(hash, 11) &* XXH_PRIME32_1
        len = len &- 1
      end

      avalanch(hash)
    end
  end
end
