#### MIT License
####
#### Copyright (c) 2023-2024 Remilia Scarlet
#### Copyright (c) 2018 Melnik Alexander
####
#### Permission is hereby granted, free of charge, to any person obtaining a
#### copy of this software and associated documentation files (the "Software"),
#### to deal in the Software without restriction, including without limitation
#### the rights to use, copy, modify, merge, publish, distribute, sublicense,
#### and/or sell copies of the Software, and to permit persons to whom the
#### Software is furnished to do so, subject to the following conditions:
####
#### The above copyright notice and this permission notice shall be included in
#### all copies or substantial portions of the Software.
####
#### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#### IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#### FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#### AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#### LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#### FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#### DEALINGS IN THE SOFTWARE.
require "./xxhash-common"

####
#### Ported from C#:
#### https://github.com/uranium62/xxHash/tree/6b20e7f7b32dfc29e5019d3d35f5b7270f1656f3
####

module RemiLib::Digest
  class XXHash64 < XXHashInternal
    @[AlwaysInline]
    protected def self.xxh64(input : Bytes, seed : UInt64) : UInt64
      h64 : UInt64 = 0
      inputPtr : PUInt8 = input.to_unsafe
      len = input.size

      if input.size >= 32
        finish : PUInt8 = inputPtr + len
        limit : PUInt8 = finish - 31

        v1 : UInt64 = seed &+ XXH_PRIME64_1 &+ XXH_PRIME64_2
        v2 : UInt64 = seed &+ XXH_PRIME64_2
        v3 : UInt64 = seed
        v4 : UInt64 = seed &- XXH_PRIME64_1

        loop do
          {% for i in 1..4 %}
            v{{i}} = round(v{{i}}, inputPtr.unsafe_as(PUInt64).value)
            inputPtr += 8
          {% end %}
          break unless inputPtr < limit
        end

        h64 = rotl64(v1, 1) &+
                    rotl64(v2, 7) &+
                    rotl64(v3, 12) &+
                    rotl64(v4, 18)

        {% for i in 1..4 %}
          h64 = mergeRound(h64, v{{i}})
        {% end %}
      else
        h64 = seed &+ XXH_PRIME64_5
      end

      h64 = h64 &+ len
      xxhFinalize(h64, inputPtr, len)
    end

    @[AlwaysInline]
    protected def self.round(acc : UInt64, input : UInt64) : UInt64
      acc = acc &+ (input &* XXH_PRIME64_2)
      acc = rotl64(acc, 31)
      acc &* XXH_PRIME64_1
    end

    @[AlwaysInline]
    protected def self.mergeRound(acc : UInt64, val : UInt64) : UInt64
      val = round(0, val)
      acc ^= val
      (acc &* XXH_PRIME64_1) &+ XXH_PRIME64_4
    end

    @[AlwaysInline]
    protected def self.avalanch(hash : UInt64) : UInt64
      hash ^= hash.unsafe_shr(33)
      hash = hash &* XXH_PRIME64_2
      hash ^= hash.unsafe_shr(29)
      hash = hash &* XXH_PRIME64_3
      hash ^ hash.unsafe_shr(32)
    end

    @[AlwaysInline]
    protected def self.xxhFinalize(hash : UInt64, ptr : PUInt8, len : Int) : UInt64
      k1 : UInt64 = 0

      len &= 31
      while len >= 8
        k1 = round(0, ptr.unsafe_as(PUInt64).value)
        ptr += 8
        hash ^= k1
        hash = (rotl64(hash, 27) &* XXH_PRIME64_1) &+ XXH_PRIME64_4
        len = len &- 8
      end

      if len >= 4
        hash ^= ptr.unsafe_as(PUInt32).value.to_u64! &* XXH_PRIME64_1
        ptr += 4
        hash = (rotl64(hash, 23) &* XXH_PRIME64_2) &+ XXH_PRIME64_3
        len = len &- 4
      end

      while len > 0
        hash ^= ptr.value.to_u64! &* XXH_PRIME64_5
        ptr += 1
        hash = rotl64(hash, 11) &* XXH_PRIME64_1
        len = len &- 1
      end

      avalanch(hash)
    end
  end
end
