#### libremiliacr
#### Copyright(C) 2020-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify
#### it under the terms of the GNU General Public License as published
#### the Free Software Foundation, either version 3 of the License, or
#### (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
#### GNU General Public License for more details.
####
#### You should have received a copy of the GNU General Public License
#### along with this program.If not, see<http:####www.gnu.org/licenses/.>

module RemiMath
  extend self

  {% for typ in [:Array, :Slice] %}
    # Implements a multiply-accumulate operation.
    @[AlwaysInline]
    def multiplyAdd(a : T, x : {{typ.id}}(T), dest : {{typ.id}}(T)) forall T
      dest.size.times do |i|
        dest[i] += a * x[i]
      end
    end

    # Implements a multiply-accumulate operation.  This is an unsafe version that
    # uses `Array#unsafe_fetch` and `Array#unsafe_put` internally for greater
    # performance.
    @[AlwaysInline]
    def multiplyAdd!(a : T, x : {{typ.id}}(T), dest : {{typ.id}}(T)) forall T
      dest.size.times do |i|
        dest.unsafe_put(i, dest.unsafe_fetch(i) + a * x.unsafe_fetch(i))
      end
    end

    # Implements a multiply-accumulate operation over two arrays at the same
    # time.
    @[AlwaysInline]
    def multiplyAdd(a : T, x1 : {{typ.id}}(T), x2 : {{typ.id}}(T),
                    dest1 : {{typ.id}}(T), dest2 : {{typ.id}}(T)) forall T
      dest1.size.times do |i|
        dest1[i] += a * x1[i]
        dest2[i] += a * x2[i]
      end
    end

    # Implements a multiply-accumulate operation over two arrays at the same
    # time.  This is an unsafe version that uses `Array#unsafe_fetch` and
    # `Array#unsafe_put` internally for greater performance.
    @[AlwaysInline]
    def multiplyAdd!(a : T, x1 : {{typ.id}}(T), x2 : {{typ.id}}(T),
                     dest1 : {{typ.id}}(T), dest2 : {{typ.id}}(T)) forall T
      dest1.size.times do |i|
        dest1.unsafe_put(i, dest1.unsafe_fetch(i) + a * x1.unsafe_fetch(i))
        dest2.unsafe_put(i, dest2.unsafe_fetch(i) + a * x2.unsafe_fetch(i))
      end
    end

    # Implements a multiply-accumulate operation that applies a step to the
    # accumulator `a` each iteration.
    @[AlwaysInline]
    def multiplyAdd(a : T, step : T, x : {{typ.id}}(T), dest : {{typ.id}}(T)) forall T
      dest.size.times do |i|
        dest[i] += a * x[i]
        a += step
      end
    end

    # Implements a multiply-accumulate operation that applies a step to the
    # accumulator `a` each iteration.  This is an unsafe version that uses
    # `Array#unsafe_fetch` and `Array#unsafe_put` internally for greater
    # performance.
    @[AlwaysInline]
    def multiplyAdd!(a : T, step : T, x : {{typ.id}}(T), dest : {{typ.id}}(T)) forall T
      dest.size.times do |i|
        dest.unsafe_put(i, dest.unsafe_fetch(i) + a * x.unsafe_fetch(i))
        a += step
      end
    end
  {% end %}
end
